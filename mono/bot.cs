#warning Build 0.6.7 LeCoc-Sportif
#warning introduce a funny switch track to simulate 3 lanes becoming 2 or something like that
using System;
using System.Linq;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Threading;

public class Bot : IDisposable
{
    #region Constants
    #endregion

    #region Events Declarations
    #endregion

    #region Variable Declarations
    private TimeSpan _marker;
    private Stopwatch _timer = Stopwatch.StartNew();
    /// <summary>
    /// Determines if the bot has detected a slip and used it for it's calculations
    /// </summary>
    private bool _hasSlipped = false;
    #region TCP/IP Stuff
    /// <summary>
    /// The stream to read/write data to the server
    /// </summary>
    private NetworkStream _network;
    /// <summary>
    /// Reads data from the server
    /// </summary>
    private StreamReader _reader;
    /// <summary>
    /// Writes data to the server
    /// </summary>
    private StreamWriter _writer;
    /// <summary>
    /// Manages the socket connection
    /// </summary>
    private TcpClient _tcpClient;
    #endregion
    #endregion

    #region Constructors, Destructors & Disposal
    /// <summary>
    /// Creates a bot that does not logging and doesn't connect to a remote host
    /// </summary>
    public Bot()
    {
        this.IsRunning = true;
        this.LastSendTick = -1;
        this.Log = new Logger();
        this.LogDiagnostics = new LogDiagnostics();
    }
    /// <summary>
    /// Creates a bot that connects to a remote host
    /// </summary>
    /// <param name="host"></param>
    /// <param name="port"></param>
    /// <param name="botName"></param>
    /// <param name="botKey"></param>
    /// <param name="logger"></param>
    /// <param name="diagLogger"></param>
    public Bot(string host, int port, string botName, string botKey, Logger logger, LogDiagnostics diagLogger)
    {
        this.IsRunning = true;
        this.LastSendTick = -1;
        this.Host = host;
        this.Port = port;
        this.BotName = botName;
        this.BotKey = botKey;
        this.Log = logger;
        this.LogDiagnostics = diagLogger;
        this.Log.Write("My name is:{0} ({1}) and I will attempt to connect to {2}:{3}", this.BotName, this.BotKey, this.Host, this.Port);
    }
    /// <summary>
    /// Creates a bot that connects to a remote host
    /// </summary>
    /// <param name="host"></param>
    /// <param name="port"></param>
    /// <param name="botName"></param>
    /// <param name="botKey"></param>
    /// <param name="logger"></param>
    /// <param name="diagLogger"></param>
    public Bot(string host, int port, string botName, string botKey, Logger logger, LogDiagnostics diagLogger, string trackName, string password, int cars)
    {
        this.IsRunning = true;
        this.LastSendTick = -1;
        this.TrackName = trackName;
        this.Password = password;
        this.CarCount = cars;
        this.Host = host;
        this.Port = port;
        this.BotName = botName;
        this.BotKey = botKey;
        this.Log = logger;
        this.LogDiagnostics = diagLogger;
        this.Log.Write("My name is:{0} ({1}) and I will attempt to connect to {2}:{3}", this.BotName, this.BotKey, this.Host, this.Port);
    }
    /// <summary>
    /// Releases the resources for the bot
    /// </summary>
    public void Dispose()
    {
        this.Disconnect();
        if (this.Log != null)
        {
            this.Log.Dispose();
            this.Log = null;
        }
        if (this.LogDiagnostics != null)
        {
            this.LogDiagnostics.Dispose();
            this.LogDiagnostics = null;
        }
    }
    #endregion

    #region Public Properties
    /// <summary>
    /// The name of the track to use
    /// </summary>
    public string TrackName { get; set; }
    /// <summary>
    /// The password for the race
    /// </summary>
    public string Password { get; set; }
    /// <summary>
    /// The number of cars
    /// </summary>
    public int CarCount { get; set; }
    /// <summary>
    /// Determines if a crash was detected during this race
    /// </summary>
    public bool CrashDetected { get; set; }
    /// <summary>
    /// Is there a tournament running
    /// </summary>
    public bool IsRunning { get; set; }
    /// <summary>
    /// Determines if the race has started
    /// </summary>
    public static int RaceStarted { get; set; }
    /// <summary>
    /// The server that I am connecting to
    /// </summary>
    public string Host { get; private set; }
    /// <summary>
    /// The port on the server to connect to
    /// </summary>
    public int Port { get; private set; }
    /// <summary>
    /// The name of the bot
    /// </summary>
    public string BotName { get; private set; }
    /// <summary>
    /// The bot's unique key
    /// </summary>
    public string BotKey { get; private set; }
    /// <summary>
    /// The log to write to
    /// </summary>
    public Logger Log { get; private set; }
    /// <summary>
    /// Logs the diagnostics of the car
    /// </summary>
    public LogDiagnostics LogDiagnostics { get; private set; }
    public ControlSystems.Controller Controls { get; set; }
    /// <summary>
    /// The last tick that a message was responded to
    /// </summary>
    public int LastSendTick { get; set; }
    #endregion

    #region Public Methods
#if !VISUAL_STUDIO_2012    
    private static StreamWriter _log = null;
    public static void Main(string[] args) {
	    string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];		
        Race(host, port, botName, botKey);		
	}
#endif
    /// <summary>
    /// Starts a race using the specified settings
    /// </summary>
    /// <param name="host"></param>
    /// <param name="port"></param>
    /// <param name="botName"></param>
    /// <param name="botKey"></param>
    public static void Race(string host, int port, string botName, string botKey)
    {
        using (Logger logger = new Logger(botName))
        using (LogDiagnostics diagLogger = new LogDiagnostics(botName))
        using (Bot bot = new Bot(host, port, botName, botKey, logger, diagLogger))
        {
            bot.RaceLoop();
        }
    }
    /// <summary>
    /// Process a message and return false when done
    /// </summary>
    public bool ProcessMessage(string line)
    {
        this.Log.Write("Received: {0}", line);
        Messages.MsgWrapper msg = JsonConvert.DeserializeObject<Messages.MsgWrapper>(line);
        switch (msg.msgType)
        {
            case "error":
                {
                    Error.RootObject root = JsonConvert.DeserializeObject<Error.RootObject>(line);
                    if (root.msgType.Contains("Color") && (root.msgType.Contains("already taken")))
                    {
                        //pick a different color
                    }
                    break;
                }
            case "spawn":
                {
                    ControlSystems.Physics.Instance.Respawn();
                    Spawn.RootObject root = JsonConvert.DeserializeObject<Spawn.RootObject>(line);
                    ControlSystems.Physics.Instance.UpdateTurbo(root.gameTick);
                    if (root.data.name == this.BotName)
                    {
                        this.Controls.Throttle = 0d;
                    }
                    break;
                }
            case "yourCar":
                {
                    YourCar.RootObject root = JsonConvert.DeserializeObject<YourCar.RootObject>(line);
                    this.BotName = root.data.name;
                    break;
                }
            case "lapFinished":
                {
                    LapFinished.RootObject root = JsonConvert.DeserializeObject<LapFinished.RootObject>(line);
                    ControlSystems.Physics.Instance.UpdateTurbo(root.gameTick);
                    if (root.data.car.name == this.BotName)
                    {
                        this.Controls.LapTimes.ResetTimer(root.data);
                    }
                    break;
                }
            case "turboAvailable":
                {
                    TurboAvailable.RootObject root = JsonConvert.DeserializeObject<TurboAvailable.RootObject>(line);
                    ControlSystems.Physics.Instance.RegisterTurbo(root.data.turboDurationTicks, root.data.turboFactor);
                    break;
                }
            case "carPositions":
                {
                    CarPositions.RootObject root = JsonConvert.DeserializeObject<CarPositions.RootObject>(line);
                    this.Controls.GameTick = root.gameTick;
                    //Console.WriteLine("Updated for tick " + root.gameTick);
                    ControlSystems.Physics.Instance.UpdateTurbo(root.gameTick);
                    foreach (CarPositions.Datum datum in root.data)
                    {
                        if (datum.id.name == this.BotName)
                        {
                            //this is my car
                            //datum.piecePosition.lane
                            this.Controls.SetPosition(datum);
                            this.Controls.LapTimes.CalculateCurrentLaptime();
                            this.MakeDecision();
                            this.LogDiagnostics.Write(root.gameTick, this.Controls.CurrentPiece.Current.Index, this.Controls.CurrentPiece.CurrentLane.SingleSection.Index, this.Controls.CurrentPiece.Current.TrackType, this.Controls.Throttle, this.Controls.Diagnostics.CurrentVelocity, this.Controls.Diagnostics.NextPredictedVelocity, this.Controls.CurrentPiece.DistanceToNextSection, this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumAttackSpeed, this.Controls.Diagnostics.Angle, this.Controls.Diagnostics.NextPredictedAngle, this.CalculateMaxAngle(this.Controls.CurrentPiece.DistanceToNextSection), this.Controls.Diagnostics.Drift, this.Controls.CurrentPiece.CurrentLane.Radius, this.Controls.CurrentPiece.Current.Angle, false);
                        }
                        else
                        {
                            //this is another car
                            ControlSystems.OtherCars.Instance.Update(this, datum);
                        }
                    }
                    //if (this.HasTheRaceStarted == false)
                    //{
                    //    SendMessage(new Messages.Ping());
                    //}
                    //else
                    {
                        SendMessage(new Messages.Throttle(this.Controls.Throttle));
                    }
                    break;
                }
            case "crash":
                {
                    Crash.RootObject root = JsonConvert.DeserializeObject<Crash.RootObject>(line);
                    ControlSystems.Physics.Instance.UpdateTurbo(root.gameTick);
                    if (root.data.name == this.BotName)
                    {
#if VISUAL_STUDIO_2012
                        File.AppendAllText(string.Format("Crashes-{0}.txt", this.TrackName), string.Format("{0:yyyy-MM-dd HH:mm:ss} on {1}", DateTime.Now.ToString(), root.gameTick));
#endif
                        this.CrashDetected = true;
                        this.Controls.Crash();
                        ControlSystems.Physics.Instance.Crash();
                        this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumAttackSpeed = 0d;
                        this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumAchievedSpeed = 0d;
                        this.LogDiagnostics.Write(root.gameTick, this.Controls.CurrentPiece.Current.Index, this.Controls.CurrentPiece.CurrentLane.SingleSection.Index, this.Controls.CurrentPiece.Current.TrackType, this.Controls.Throttle, this.Controls.Diagnostics.CurrentVelocity, this.Controls.Diagnostics.NextPredictedVelocity, this.Controls.CurrentPiece.DistanceToNextSection, this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumAttackSpeed, this.Controls.Diagnostics.Angle, this.Controls.Diagnostics.NextPredictedAngle, 0d, this.Controls.Diagnostics.Drift, this.Controls.CurrentPiece.CurrentLane.Radius, this.Controls.CurrentPiece.Current.Angle, true);
                    }
                    SendMessage(new Messages.Ping());
                    break;
                }
            case "join":
                {
                    SendMessage(new Messages.Ping());
                    break;
                }
            case "gameInit":
                {
                    GameInit.GameInit init = JsonConvert.DeserializeObject<GameInit.GameInit>(line);
                    ControlSystems.TrackController.Instance.Initialize(init);
                    ControlSystems.OtherCars.Instance.Reset();
                    this.Controls = new ControlSystems.Controller(this);
                    SendMessage(new Messages.Ping());
                    break;
                }
            case "gameEnd":
                {
                    this.Log.Write("Race ended");
                    SendMessage(new Messages.Ping());
                    break;
                }
            case "gameStart":
                {
                    RaceStarted++;
                    this.Log.Write("Race starts");
                    SendMessage(new Messages.Ping());
                    break;
                }
            case "tournamentEnd":
                {
                    this.IsRunning = false;
                    break;
                }
            default:
                {
                    SendMessage(new Messages.Ping());
                    break;
                }
        }
        return true;
    }
    /// <summary>
    /// Sends a message to the server
    /// </summary>
    /// <param name="msg"></param>
    public void SendMessage(Messages.SendMsg msg)
    {
        try
        {
            //if ((this.Controls != null) && (this.LastSendTick == this.Controls.GameTick))
            //{
            //    Log.Write("Not Sending: {0}", msg.ToJson().ToString());
            //    return;
            //}
            if (this._writer == null)
            {
                Log.Write("I am no longer connected!!");
                return;
            }
            Log.Write("Sending: {0}", msg.ToJson().ToString());
            if (this.Controls != null)
            {
                this.LastSendTick = this.Controls.GameTick;
            }
            this._writer.WriteLine(msg.ToJson());
        }
        catch
        {
        }
    }
    /// <summary>
    /// Runs the tight loop that performs the race
    /// </summary>
    public void RaceLoop()
    {
        if (this.Connect() == true)
        {
            if (this.CarCount == 0)
            {
                this.SendMessage(new Messages.Join(this.BotName, this.BotKey));
            }
            else
            {
                this.SendMessage(new Messages.JoinRace(this.BotName, this.BotKey, this.TrackName, this.Password, this.CarCount));
            }
            while (this.ReceiveMessage() == true)
            {
            }
            this.Log.Flush();
            this.LogDiagnostics.Flush();
        }
    }
    #endregion

    #region Internal Methods
    #endregion

    #region Private Methods
    /// <summary>
    /// Makes the decision to do something
    /// </summary>
    private void MakeDecision()
    {
        if (ControlSystems.Physics.Instance.Completed == false)
        {
            this.Controls.Throttle = 1.0d;
            if (this.Controls.Diagnostics.CurrentVelocity > 0d)
            {
                ControlSystems.Physics.Instance.RegisterVelocity(this.Controls.Diagnostics.CurrentVelocity, this.Controls.Throttle);
            }
        }
        else
        {
            this.AI();
            this.Controls.Diagnostics.NextPredictedVelocity = ControlSystems.Physics.Instance.CalculateSpeed(this.Controls.Diagnostics.CurrentVelocity, 1, this.Controls.Throttle);
            //this.ProctectAgainstCrashes();
        }
    }
    /// <summary>
    /// This is a hard limit fix to prevent me from slipping. I might need this to be more strict??
    /// </summary>
    private void ProctectAgainstCrashes()
    {
        if (this.Controls.Diagnostics.Drift == ControlSystems.Diagnostics.DriftAngles.GettingWorse)
        {
            if ((Math.Abs(this.Controls.Diagnostics.Angle) >= 0) && (this.Controls.Diagnostics.DriftValue >= 5d))
            {
                this.Controls.Throttle = 0d;
            }
            else if ((Math.Abs(this.Controls.Diagnostics.Angle) >= 10) && (this.Controls.Diagnostics.DriftValue >= 5d))
            {
                this.Controls.Throttle = 0d;
            }
            else if ((Math.Abs(this.Controls.Diagnostics.Angle) >= 20) && (this.Controls.Diagnostics.DriftValue >= 4d))
            {
                this.Controls.Throttle = 0d;
            }
            else if ((Math.Abs(this.Controls.Diagnostics.Angle) >= 25) && (this.Controls.Diagnostics.DriftValue >= 3d))
            {
                this.Controls.Throttle = 0d;
            }
            else if ((Math.Abs(this.Controls.Diagnostics.Angle) >= 46) && (this.Controls.Diagnostics.DriftValue >= 2d))
            {
                this.Controls.Throttle = 0d;
            }
            else if ((Math.Abs(this.Controls.Diagnostics.Angle) >= 55) && (this.Controls.Diagnostics.DriftValue >= 1d))
            {
                this.Controls.Throttle = 0d;
            }
            else if (Math.Abs(this.Controls.Diagnostics.Angle) >= 58)
            {
                this.Controls.Throttle = 0d;
            }
        }
    }
    /// <summary>
    /// My normal free racing code
    /// </summary>               
    private void AI()
    {
        if ((Math.Abs(this.Controls.Diagnostics.CurrentVelocity - this.Controls.Diagnostics.NextPredictedVelocity) < 0.001) && (this.Controls.Diagnostics.NextPredictedVelocity > 0))
        {
        }
        double percentageRemaining = this.Controls.CurrentPiece.DistanceToNextSection / this.Controls.CurrentPiece.CurrentLane.SingleSection.TotalLength;
        if ((ControlSystems.Physics.Instance.TurboAvailable == true) && (this.Controls.CurrentPiece.Current.TrackType == ControlSystems.TrackTypes.Straight) && (percentageRemaining >= 0.8))
        {
            foreach (var lane in this.Controls.CurrentPiece.Current.Lanes)
            {
                if ((this.LastSendTick != this.Controls.GameTick) &&
                (ControlSystems.TrackController.SingleSection.LongestStraight.Index == lane.SingleSection.Index)
                )
                {
                    ControlSystems.Physics.Instance.EnableTurbo(this, this.Controls.GameTick);
                }
            }
        }
        if ((this.Controls.CurrentPiece.Current.TrackType == ControlSystems.TrackTypes.Straight) || (this.Controls.CurrentPiece.Current.TreatAsStraight == true))
        {
            ControlSystems.TrackController.TrackSection nextSection = this.Controls.CurrentPiece.Current.NextSection;
            while (nextSection.TrackType == this.Controls.CurrentPiece.Current.TrackType)
            {
                nextSection = nextSection.NextSection;
            }
            #region Straight
            //double sampleSpeed = 0d;
            //double maxAnticipatedAngle = this.CalculateMaxAngle(nextSection.Radius, out sampleSpeed);
            //if (maxAnticipatedAngle > 60)
            //{
            //    int requiredTicks = ControlSystems.Physics.Instance.CalculateRequiredTicks(this.Controls.Diagnostics.CurrentVelocity, sampleSpeed, 0d);
            //    double distance = ControlSystems.Physics.Instance.CalculateDistanceTraveled(this.Controls.Diagnostics.CurrentVelocity, requiredTicks, 0);
            //    if ((ControlSystems.Physics.Instance.IsTurboActive == true) && (nextSection.TreatAsStraight == true))
            //    {
            //        distance /= 3;
            //    }
            //    if ((distance > this.Controls.CurrentPiece.DistanceToNextSection) && (nextSection.TreatAsStraight == false))
            //    {
            //        this.Controls.Throttle = 0;
            //    }
            //    else
            //    {
            //        this.Controls.Throttle = 1.0d;
            //    }
            //}
            //else
            //{
            //    this.Controls.Throttle = 1.0d;
            //}         
            if (this.Controls.GameTick >= 1441)
            {
            }
            int requiredTicks = ControlSystems.Physics.Instance.CalculateRequiredTicks(this.Controls.Diagnostics.CurrentVelocity, this.Controls.Diagnostics.MaxTurnAttackSpeed, 0d);
            double distance = ControlSystems.Physics.Instance.CalculateDistanceTraveled(this.Controls.Diagnostics.CurrentVelocity, requiredTicks, 0);
            if ((ControlSystems.Physics.Instance.IsTurboActive == true) && (nextSection.TreatAsStraight == true))
            {
                //distance /= 3;
            }
            if ((Math.Abs(this.Controls.Diagnostics.Angle) > 45) && (this.Controls.Diagnostics.Drift == ControlSystems.Diagnostics.DriftAngles.GettingWorse))
            {
                this.Controls.Throttle = 0d;
            }
            else if ((ControlSystems.Physics.Instance.IsTurboActive == true) && (distance * 2 > this.Controls.CurrentPiece.DistanceToNextSection))// && (nextSection.TreatAsStraight == false))
            {
                this.Controls.Throttle = 0d;
            }
            else if ((distance + this.Controls.Diagnostics.CurrentVelocity > this.Controls.CurrentPiece.DistanceToNextSection) && (nextSection.TreatAsStraight == false))
            {
                this.Controls.Throttle = 0d;
            }
            else
            {
                if (ControlSystems.Physics.Instance.IsTurboActive == true)
                {
                    if (ControlSystems.Physics.Instance.CanIKeepThrottleWithTurbo(this) == false)
                    {
                        this.Controls.Throttle = 0d;
                    }
                    else
                    {
                        this.Controls.Throttle = 1d;
                    }
                }
                else if (Math.Abs(this.Controls.CurrentPiece.Current.Angle) > 0)
                {
                    double maxAnticipatedAngle = this.CalculateMaxAngle(this.Controls.CurrentPiece.DistanceToNextSection);
                    if (maxAnticipatedAngle > 40)
                    {
                        this.Controls.Throttle = 0d;
                    }
                    else
                    {
                        this.Controls.Throttle = 1.0d;
                    }
                }
                else
                {
                    this.Controls.Throttle = 1.0d;
                }
            }
            //int requiredTicks = ControlSystems.Physics.Instance.CalculateRequiredTicks(this.Controls.Diagnostics.CurrentVelocity, this.Controls.Diagnostics.MaxTurnAttackSpeed * 1.1d, 0d);
            //double distance = ControlSystems.Physics.Instance.CalculateDistanceTraveled(this.Controls.Diagnostics.CurrentVelocity, requiredTicks, 0);
            //if ((ControlSystems.Physics.Instance.IsTurboActive == true) && (nextSection.TreatAsStraight == true))
            //{
            //    distance /= 3;
            //}
            //if ((distance > this.Controls.CurrentPiece.DistanceToNextSection) && (nextSection.TreatAsStraight == false))
            //{
            //    this.Controls.Throttle = 0;
            //}
            //else
            //{
            //    this.Controls.Throttle = 1.0d;
            //}
            #endregion
        }
        else
        {
            ControlSystems.TrackController.TrackSection nextSection = this.Controls.CurrentPiece.Current.NextSection;
            while ((nextSection.TrackType == this.Controls.CurrentPiece.Current.TrackType) &&
                (nextSection.Radius == this.Controls.CurrentPiece.Current.Radius))
            {
                nextSection = nextSection.NextSection;
            }            
            int requiredTicks = ControlSystems.Physics.Instance.CalculateRequiredTicks(this.Controls.Diagnostics.CurrentVelocity, this.Controls.Diagnostics.MaxTurnAttackSpeed, 0d);
            double distance = ControlSystems.Physics.Instance.CalculateDistanceTraveled(this.Controls.Diagnostics.CurrentVelocity, requiredTicks, 0);
            if ((nextSection.Radius < 50) &&
                (distance + this.Controls.Diagnostics.CurrentVelocity > this.Controls.CurrentPiece.DistanceToNextSection) 
                && (nextSection.TreatAsStraight == false))
            {
                //I need to reduce speed so I can make the next turn
                this.Controls.Throttle = 0d;
            }
            else
            {
                this.AttemptToGoFastAroundCorners();
            }
            //this.CalculateTurning();
        }
    }
    /// <summary>
    /// Connects the bot to the server
    /// </summary>
    private bool Connect()
    {
        this.Log.Write("Connecting...");
        try
        {
            this._tcpClient = new TcpClient(this.Host, this.Port);
            this.Log.Write("Connected!");
            this._network = this._tcpClient.GetStream();
            this._reader = new StreamReader(this._network);
            this._writer = new StreamWriter(this._network);
            this._writer.AutoFlush = true;
            return true;
        }
        catch (Exception ex)
        {
            this.Disconnect();
            this.Log.Write(ex);
        }
        this.Log.Write("Connection attempt failed!");
        return false;
    }
    /// <summary>
    /// Disconnects the streams and TCP Client
    /// </summary>
    private void Disconnect()
    {
        if (this._reader != null)
        {
            try
            {
                this._reader.Dispose();
            }
            catch
            {
            }
            this._reader = null;
        }
        if (this._writer != null)
        {
            try
            {
                this._writer.Dispose();
            }
            catch
            {
            }
            this._writer = null;
        }
        if (this._network != null)
        {
            try
            {
                this._network.Dispose();
            }
            catch
            {
            }
            this._network = null;
        }
        if (this._tcpClient != null)
        {
            try
            {
                ((IDisposable)this._tcpClient).Dispose();
            }
            catch
            {
            }
            this._tcpClient = null;
        }
    }
    /// <summary>
    /// Receives a message from the remote server
    /// </summary>
    /// <returns></returns>
    private bool ReceiveMessage()
    {
        try
        {
            string line = this._reader.ReadLine();
            this._marker = this._timer.Elapsed;
            if (line == null)
            {
                return false;
            }
            bool output = this.ProcessMessage(line);
            this._marker = this._timer.Elapsed.Subtract(this._marker);
            this.Log.Write("Latency: {0}", this._marker);
            return output;
        }
        catch
        {
            return false;
        }
    }
    private void AttemptToGoFastAroundCorners()
    {
        if ((this._hasSlipped == false) && (Math.Abs(this.Controls.Diagnostics.Angle) != 0))
        {
            #region Run this once to calculate the angles
            this._hasSlipped = true;
            //double V = this.Controls.Diagnostics.CurrentVelocity;
            //double R = this.Controls.CurrentPiece.CurrentLane.Radius;
            //double A = this.Controls.Diagnostics.Angle;

            //double V = 7;
            //double R = 110;
            //double A = 0.45;

            //double B = RadianToDegrees(V / R);
            //double Bslip = B - A;
            //double Vthresh = DegreeToRadian(Bslip) * R;
            ////this is my friction cofficient!
            //double Fslip = (Vthresh * Vthresh) / R;

            //double Vmaxnoslip = Math.Sqrt(Fslip * 90);

            //double variance = 0.190913830859396d - this.Controls.Diagnostics.Angle;
            //this._hasSlipped = true;
            this.Controls.Diagnostics.CalculateFrictionCoefficient(this.Controls.Diagnostics.CurrentVelocity, this.Controls.Diagnostics.Angle, this.Controls.CurrentPiece.CurrentLane.Radius);
            //now also calculate the maximum speeds for each of the linked sections            
            foreach (var piece in ControlSystems.TrackController.Instance.Sections)
            {
                foreach (var lane in piece.Lanes)
                {
                    if (lane.SingleSection.MaximumSpeedBeforeSlipping == 0d)
                    {
                        lane.SingleSection.MaximumSpeedBeforeSlipping = Math.Sqrt(ControlSystems.Diagnostics.FrictionCoefficient * lane.Radius);
                    }
                }
            }
            #endregion
        }
        else if (this._hasSlipped == false)
        {
            this.Controls.Throttle = 0.7d;
        }
        else
        {            
            if ((this.Controls.CurrentPiece.Current.TrackType == ControlSystems.TrackTypes.RightTurn)
                && ((this.Controls.Diagnostics.Angle < 0) || (this.Controls.Diagnostics.PreviousAngle < 0))
                && this.Controls.CurrentPiece.CurrentLane.SingleSection.TotalLength > 50)
            {
                double maxAnticipatedAngle = 0d;
                if (this.Controls.CurrentPiece.Current.NextSection.Lanes[this.Controls.CurrentPiece.CurrentLane.Index].Length < 100)
                {
                    maxAnticipatedAngle = this.CalculateMaxAngle(Math.Abs(this.Controls.CurrentPiece.DistanceToNextSection) + this.Controls.CurrentPiece.Current.NextSection.Lanes[this.Controls.CurrentPiece.CurrentLane.Index].Length);
                }
                else
                {
                    maxAnticipatedAngle = this.CalculateMaxAngle(Math.Abs(this.Controls.CurrentPiece.DistanceToNextSection));
                }
                if (maxAnticipatedAngle > 50)
                {
                    this.Controls.Throttle = 0d;
                }
                else
                {
                    if (Math.Abs(this.Controls.Diagnostics.DriftValue) < 1)
                    {
                        this.Controls.Throttle = 1.0d;
                    }
                    if ((this.Controls.Diagnostics.CurrentVelocity > this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumAchievedSpeed)
                        && (this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumAchievedSpeed > 0))
                    {
                        //try to go at the maximum achieved speed
                        //this causes a problem as I sometimes slip and go off track
                        //this.Controls.Throttle = ControlSystems.Physics.Instance.CalculateThrottleRequired(this.Controls.Diagnostics.CurrentVelocity, this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumAchievedSpeed);
                        this.Controls.Throttle = 0d;
                    }                    
                    else if ((this.Controls.Diagnostics.CurrentVelocity > this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumAttackSpeed)
                        && (this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumAttackSpeed > 0))
                    {
                        this.Controls.Throttle = 0d;
                    }
                    else if ((this.Controls.Diagnostics.CurrentVelocity > this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumSpeedBeforeSlipping)
                        && (this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumSpeedBeforeSlipping > 0)
                        && (Math.Abs(this.Controls.Diagnostics.Angle) > 20))
                    {
                        ///try to go at the maximum speed before slipping
                        this.Controls.Throttle = ControlSystems.Physics.Instance.CalculateThrottleRequired(this.Controls.Diagnostics.CurrentVelocity, this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumSpeedBeforeSlipping);
                    }
                    else
                    {
                        this.Controls.Throttle = ControlSystems.Physics.Instance.CalculateThrottleRequired(this.Controls.Diagnostics.CurrentVelocity, this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumSpeedBeforeSlipping * 1.5d);
                    }
                }                
            }
            else if ((this.Controls.CurrentPiece.Current.TrackType == ControlSystems.TrackTypes.LeftTurn)
                && ((this.Controls.Diagnostics.Angle > 0) || (this.Controls.Diagnostics.PreviousAngle > 0))
                && this.Controls.CurrentPiece.CurrentLane.SingleSection.TotalLength > 50)
            {
                double maxAnticipatedAngle = 0d;
                if (this.Controls.CurrentPiece.Current.NextSection.Lanes[this.Controls.CurrentPiece.CurrentLane.Index].Length < 100)
                {
                    maxAnticipatedAngle = this.CalculateMaxAngle(Math.Abs(this.Controls.CurrentPiece.DistanceToNextSection) + this.Controls.CurrentPiece.Current.NextSection.Lanes[this.Controls.CurrentPiece.CurrentLane.Index].Length);
                }
                else
                {
                    maxAnticipatedAngle = this.CalculateMaxAngle(Math.Abs(this.Controls.CurrentPiece.DistanceToNextSection));
                }
                if (maxAnticipatedAngle > 50)
                {
                    this.Controls.Throttle = 0d;
                }
                else
                {
                    if (Math.Abs(this.Controls.Diagnostics.DriftValue) < 1)
                    {
                        this.Controls.Throttle = 1.0d;
                    }
                    if ((this.Controls.Diagnostics.CurrentVelocity > this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumAchievedSpeed)
                        && (this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumAchievedSpeed > 0))
                    {
                        //try to go at the maximum achieved speed
                        //this causes a problem as I sometimes slip and go off track
                        //this.Controls.Throttle = ControlSystems.Physics.Instance.CalculateThrottleRequired(this.Controls.Diagnostics.CurrentVelocity, this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumAchievedSpeed);
                        this.Controls.Throttle = 0d;
                    }
                    else if ((this.Controls.Diagnostics.CurrentVelocity > this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumAttackSpeed)
                    && (this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumAttackSpeed > 0))
                    {
                        this.Controls.Throttle = 0d;
                    }
                    else if ((this.Controls.Diagnostics.CurrentVelocity > this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumSpeedBeforeSlipping)
                        && (this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumSpeedBeforeSlipping > 0)
                        && (Math.Abs(this.Controls.Diagnostics.Angle) > 20))
                    {
                        ///try to go at the maximum speed before slipping
                        this.Controls.Throttle = ControlSystems.Physics.Instance.CalculateThrottleRequired(this.Controls.Diagnostics.CurrentVelocity, this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumSpeedBeforeSlipping);
                    }                    
                    else
                    {
                        this.Controls.Throttle = ControlSystems.Physics.Instance.CalculateThrottleRequired(this.Controls.Diagnostics.CurrentVelocity, this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumSpeedBeforeSlipping * 1.5d);
                    }
                }                
            }
            else
            {
                #region REMOVED!! only do this on the first tick in a turn, if I see slipping
                //if ((this.Controls.Diagnostics.CurrentVelocity > this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumAttackSpeed) &&
                //    (this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumAttackSpeed > 0))
                //{
                //    this.Controls.Throttle = 0d;
                //    return;
                //}
                #endregion

                while ((Math.Abs(this.Controls.Diagnostics.NextPredictedAngle) < 55) && (this.Controls.Throttle < 1.0d))
                {
                    this.Controls.Throttle += 0.001;
                }
                double maxAnticipatedAngle = 0d;
                if (this.Controls.CurrentPiece.Current.NextSection.Lanes[this.Controls.CurrentPiece.CurrentLane.Index].Length < 100)
                {
                    maxAnticipatedAngle = this.CalculateMaxAngle(Math.Abs(this.Controls.CurrentPiece.DistanceToNextSection) + this.Controls.CurrentPiece.Current.NextSection.Lanes[this.Controls.CurrentPiece.CurrentLane.Index].Length);
                }
                else
                {
                    maxAnticipatedAngle = this.CalculateMaxAngle(Math.Abs(this.Controls.CurrentPiece.DistanceToNextSection));
                }
                if ((maxAnticipatedAngle >= 55) && (this.Controls.Diagnostics.Drift != ControlSystems.Diagnostics.DriftAngles.GettingBetter)
                    || (maxAnticipatedAngle > 100))
                {
                    this.Controls.Throttle = 0d;
                }
                else if (this.Controls.Diagnostics.Drift == ControlSystems.Diagnostics.DriftAngles.GettingBetter)
                {
                    if (this.Controls.Diagnostics.CurrentVelocity > this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumSpeedBeforeSlipping * 1.5d)
                    {
                        this.Controls.Throttle = 0.0d;
                    }
                    else if (Math.Abs(this.Controls.Diagnostics.Angle) < 40)
                    {
                        this.Controls.Throttle = 1.0d;
                    }
                    else
                    {
                        this.Controls.Throttle = ControlSystems.Physics.Instance.CalculateThrottleRequired(this.Controls.Diagnostics.CurrentVelocity, this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumSpeedBeforeSlipping * 1.2d);
                    }
                    //if (this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumAchievedSpeed > 0)
                    //{
                    //    this.Controls.Throttle = ControlSystems.Physics.Instance.CalculateThrottleRequired(this.Controls.Diagnostics.CurrentVelocity, this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumAchievedSpeed);
                    //}
                    //else if (Math.Abs(this.Controls.Diagnostics.Angle) < 40)
                    //{
                    //    this.Controls.Throttle = 1.0d;
                    //}
                    //else
                    //{
                    //    this.Controls.Throttle = ControlSystems.Physics.Instance.CalculateThrottleRequired(this.Controls.Diagnostics.CurrentVelocity, this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumSpeedBeforeSlipping * 1.2d);
                    //}
                }
                else
                {
                    if (Math.Abs(this.Controls.Diagnostics.DriftValue) < 1)
                    {
                        if (Math.Abs(maxAnticipatedAngle) < 40)
                        {
                            this.Controls.Throttle = 1.0d;
                        }
                        else if ((this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumAchievedSpeed > 0d) && (Math.Abs(maxAnticipatedAngle) < 50))
                        {
                            this.Controls.Throttle = ControlSystems.Physics.Instance.CalculateThrottleRequired(this.Controls.Diagnostics.CurrentVelocity, this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumAchievedSpeed);
                        }
                        else
                        {
                            this.Controls.Throttle = ControlSystems.Physics.Instance.CalculateThrottleRequired(this.Controls.Diagnostics.CurrentVelocity, this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumSpeedBeforeSlipping);
                        }
                    }
                    else
                    {
                        this.Controls.Throttle = ControlSystems.Physics.Instance.CalculateThrottleRequired(this.Controls.Diagnostics.CurrentVelocity, this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumSpeedBeforeSlipping);
                    }
                }
            }
            if ((this.Controls.Diagnostics.CurrentVelocity > this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumAchievedSpeed) && (this.Controls.Throttle > 0d))
            {
                this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumAchievedSpeed = this.Controls.Diagnostics.CurrentVelocity;// *0.9;
            }
        }
    }
    /// <summary>
    /// Calculates the maximum angle I will reach if I stop accelerating right now
    /// </summary>
    /// <returns></returns>
    public double CalculateMaxAngle(double distanceToNextSection)
    {
        double sampleSpeed = 0d;
        return this.CalculateMaxAngle(this.Controls.CurrentPiece.CurrentLane.Radius, out sampleSpeed, distanceToNextSection);
    }
    /// <summary>
    /// Calculates the maximum angle I will reach if I stop accelerating right now
    /// </summary>
    /// <returns></returns>
    public double CalculateMaxAngle(double radius, out double currentSpeed, double distanceToNextSection)
    {
        distanceToNextSection *= 1.1d;
        if (this.Controls.GameTick == 258)
        {
        }
        double maxSpeed = Math.Sqrt(ControlSystems.Diagnostics.FrictionCoefficient * radius);
        currentSpeed = this.Controls.Diagnostics.CurrentVelocity;

        //add 1 tick for delay
        currentSpeed = ControlSystems.Physics.Instance.CalculateSpeed(currentSpeed, 1, this.Controls.Diagnostics.PreviousThrottle);
        distanceToNextSection -= currentSpeed;
        //double throttle = ControlSystems.Physics.Instance.CalculateThrottleRequired(currentSpeed, maxSpeed);
        double throttle = 0;
        double currentAngle = this.Controls.Diagnostics.Angle;
        double previousAngle = this.Controls.Diagnostics.PreviousAngle;

        //add 1 tick for delay
        currentAngle = ControlSystems.Physics.Instance.PredictNextAngle(currentAngle, previousAngle, this.Controls.CurrentPiece.CurrentLane.Radius, currentSpeed);
        //Console.WriteLine("Speed:{0} Angle:{1}", currentSpeed, currentAngle);
        while ((currentSpeed > maxSpeed) && (maxSpeed > 0) && (distanceToNextSection > 0))
        {
            //throttle = ControlSystems.Physics.Instance.CalculateThrottleRequired(currentSpeed, maxSpeed);
            double remember = currentAngle;
            currentAngle = ControlSystems.Physics.Instance.PredictNextAngle(currentAngle, previousAngle, this.Controls.CurrentPiece.CurrentLane.Radius, currentSpeed);
            previousAngle = remember;
            //Console.WriteLine("Speed:{0} Angle:{1}", currentSpeed, currentAngle);
            currentSpeed = ControlSystems.Physics.Instance.CalculateSpeed(currentSpeed, 1, throttle);
            distanceToNextSection -= currentSpeed;
        }
        return Math.Abs(currentAngle);
    }
    /// <summary>
    /// Calculetes the turning as I did it just before the deadling
    /// </summary>
    //private void CalculateTurning()
    //{
    //    if (this.Controls.GameTick >= 1360)
    //    {
    //    }
    //    int requiredTicks = ControlSystems.Physics.Instance.CalculateRequiredTicks(this.Controls.Diagnostics.CurrentVelocity, this.Controls.Diagnostics.MaxTurnAttackSpeed, 0d);
    //    double distance = ControlSystems.Physics.Instance.CalculateDistanceTraveled(this.Controls.Diagnostics.CurrentVelocity, requiredTicks, 0);
    //    if (distance > this.Controls.CurrentPiece.DistanceToNextSection)
    //    {
    //        this.Controls.Throttle = 0;
    //    }
    //    else
    //    {
    //        double percentageRemaining = this.Controls.CurrentPiece.DistanceToNextSection / this.Controls.CurrentPiece.CurrentLane.SingleSection.TotalLength;
    //        if ((this.Controls.Diagnostics.Drift != ControlSystems.Diagnostics.DriftAngles.GettingWorse) && (Math.Abs(this.Controls.Diagnostics.Angle) > 0d))
    //        {
    //            #region Getting Better
    //            if ((Math.Abs(this.Controls.Diagnostics.Angle) < 50) && (percentageRemaining > 20))
    //            {
    //                if (this.Controls.Diagnostics.CurrentVelocity > this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumSpeed)
    //                {
    //                    this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumAngle = this.Controls.Diagnostics.Angle;
    //                    this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumSpeed = this.Controls.Diagnostics.CurrentVelocity;
    //                    if (Math.Abs(this.Controls.Diagnostics.Angle) < 50)
    //                    {
    //                        this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumSpeed *= 1.1d;
    //                    }
    //                }
    //            }
    //            if ((this.Controls.Throttle < 1.0d) && (this.Controls.Throttle > 0))
    //            {
    //                if (this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumSpeed == 0d)
    //                {
    //                    this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumAngle = Math.Abs(this.Controls.Diagnostics.Angle);
    //                    if (Math.Abs(this.Controls.Diagnostics.Angle) < 30)
    //                    {
    //                        this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumSpeed = this.Controls.Diagnostics.CurrentVelocity * 1.1d;
    //                    }
    //                    else if (Math.Abs(this.Controls.Diagnostics.Angle) < 40)
    //                    {
    //                        this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumSpeed = this.Controls.Diagnostics.CurrentVelocity * 1.05d;
    //                    }
    //                    else if (Math.Abs(this.Controls.Diagnostics.Angle) < 50)
    //                    {
    //                        this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumSpeed = this.Controls.Diagnostics.CurrentVelocity * 1.01d;
    //                    }
    //                    else
    //                    {
    //                        this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumSpeed = this.Controls.Diagnostics.CurrentVelocity;
    //                    }
    //                }
    //                else if (this.Controls.Diagnostics.CurrentVelocity > this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumSpeed)
    //                {
    //                    this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumAngle = Math.Abs(this.Controls.Diagnostics.Angle);
    //                    this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumSpeed = this.Controls.Diagnostics.CurrentVelocity;
    //                }
    //            }
    //            if (this.Controls.Diagnostics.CurrentVelocity > this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumSpeed)
    //            {
    //                this.Controls.Throttle = ControlSystems.Physics.Instance.CalculateThrottleRequired(this.Controls.Diagnostics.CurrentVelocity, this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumSpeed);
    //            }
    //            else if (Math.Abs(this.Controls.Diagnostics.Angle) < 40)
    //            {
    //                this.Controls.Throttle = 1.0d;
    //            }
    //            else
    //            {
    //                this.Controls.Throttle = 0.5d;
    //            }
    //            //if (percentageRemaining < 0.6)
    //            //{
    //            //    this.Controls.Throttle = 1.0d;
    //            //}
    //            //else
    //            //{
    //            //    this.Controls.Throttle = 0.5d;
    //            //}
    //            #endregion
    //        }
    //        else if ((Math.Abs(this.Controls.Diagnostics.Angle) < 50) && (percentageRemaining < 0.8) && (this.Controls.Diagnostics.Drift != ControlSystems.Diagnostics.DriftAngles.GettingWorse))
    //        {
    //            this.Controls.Throttle = ControlSystems.Physics.Instance.CalculateThrottleRequired(this.Controls.Diagnostics.CurrentVelocity, this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumSpeed);
    //            if ((this.Controls.Diagnostics.CurrentVelocity <= 5d) && (this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumSpeed == 0d))
    //            {
    //                this.Controls.Throttle += 1.0d;
    //            }
    //            else
    //            {
    //                this.Controls.Throttle += 0.1;
    //            }
    //        }
    //        else
    //        {
    //            if ((this.Controls.CurrentPiece.DistanceToNextSection < 30) && (Math.Abs(this.Controls.Diagnostics.Angle) < 40))
    //            {
    //                this.Controls.Throttle = 1.0d;
    //            }
    //            else
    //            {
    //                this.Controls.Throttle = ControlSystems.Physics.Instance.CalculateThrottleRequired(this.Controls.Diagnostics.CurrentVelocity, this.Controls.CurrentPiece.CurrentLane.SingleSection.MaximumSpeed);
    //            }
    //        }
    //        if ((this._hasSlipped == false) && (Math.Abs(this.Controls.Diagnostics.Angle) != 0))
    //        {
    //            #region Run this once to calculate the angles
    //            this._hasSlipped = true;
    //            //double V = this.Controls.Diagnostics.CurrentVelocity;
    //            //double R = this.Controls.CurrentPiece.CurrentLane.Radius;
    //            //double A = this.Controls.Diagnostics.Angle;

    //            //double V = 7;
    //            //double R = 110;
    //            //double A = 0.45;

    //            //double B = RadianToDegrees(V / R);
    //            //double Bslip = B - A;
    //            //double Vthresh = DegreeToRadian(Bslip) * R;
    //            ////this is my friction cofficient!
    //            //double Fslip = (Vthresh * Vthresh) / R;

    //            //double Vmaxnoslip = Math.Sqrt(Fslip * 90);

    //            //double variance = 0.190913830859396d - this.Controls.Diagnostics.Angle;
    //            //this._hasSlipped = true;
    //            this.Controls.Diagnostics.CalculateFrictionCoefficient(this.Controls.Diagnostics.CurrentVelocity, this.Controls.Diagnostics.Angle, this.Controls.CurrentPiece.CurrentLane.Radius);
    //            //now also calculate the maximum speeds for each of the linked sections
    //            ControlSystems.Hacks.Instance.Log(this);
    //            foreach (var piece in ControlSystems.TrackController.Instance.Sections)
    //            {
    //                foreach (var lane in piece.Lanes)
    //                {
    //                    if (lane.SingleSection.MaximumSpeed == 0d)
    //                    {
    //                        lane.SingleSection.MaximumSpeed = Math.Sqrt(ControlSystems.Diagnostics.FrictionCoefficient * lane.Radius);
    //                    }
    //                }
    //            }
    //            #endregion
    //        }
    //    }
    //}
    #endregion
}
#region Logging
/// <summary>
/// Performs logging
/// </summary>
public class Logger : IDisposable
{
    #region Variable Declarations
    private string _filename = "";
    /// <summary>
    /// Writes the output log
    /// </summary>
    private StreamWriter _log = null;
    #endregion

    #region Constructors, Destructors & Disposal
    /// <summary>
    /// Creates a log file that doesn't write to log
    /// </summary>    
    public Logger()
    {
    }
    /// <summary>
    /// Default constructor
    /// </summary>
    /// <param name="botName">The name of the bot</param>
    public Logger(string botName)
    {
#if VISUAL_STUDIO_2012
        _filename = string.Format("{0}-{1:yyyy-MM-dd HH.mm.ss.ffff}.log", botName, DateTime.Now);
        foreach (char c in Path.GetInvalidFileNameChars())
        {
            _filename = _filename.Replace(c.ToString(), "");
        }
        _log = new StreamWriter(_filename);
#endif
        this.Write("Log started");
    }
    /// <summary>
    /// Closes the log
    /// </summary>
    public void Dispose()
    {
        this.Write("Log stopped");
        StreamWriter old = _log;
        _log = null;
        if (old != null)
        {
            old.Flush();
            old.Close();
            old.Dispose();
        }
        if (File.Exists(this._filename) == true)
        {
            try
            {
                if (File.Exists(Path.Combine("Replay", "Current.log")) == true)
                {
                    File.Delete(Path.Combine("Replay", "Current.log"));
                }
                File.Copy(this._filename, Path.Combine("Replay", "Current.log"));
            }
            catch
            {
            }
        }
    }
    #endregion

    #region Public Methods
    /// <summary>
    /// Writes text to the log
    /// </summary>
    /// <param name="format"></param>
    /// <param name="args"></param>
    public void Write(string format, params object[] args)
    {
        //if (_log == null)
        //{            
        //    return;
        //}
        this.Write(string.Format(format, args));
    }
    /// <summary>
    /// Writes text to the log
    /// </summary>
    /// <param name="text"></param>
    public void Write(string text)
    {
        if (_log == null)
        {
#if !VISUAL_STUDIO_2012
//            Console.WriteLine("{0:yyyy-MM-dd HH:mm:ss.ffffff tt}:{1}", DateTime.Now, text.Replace("\n", "\\n"));
#endif
            return;
        }
        _log.WriteLine("{0:yyyy-MM-dd HH:mm:ss.ffffff tt}:{1}", DateTime.Now, text.Replace("\n", "\\n"));
        _log.Flush();
    }
    public void Flush()
    {
        if (_log != null)
        {
            this._log.Flush();
        }
    }
    /// <summary>
    /// Writes an exception to the log
    /// </summary>
    /// <param name="ex"></param>
    public void Write(Exception ex)
    {
        if (_log == null)
        {
            return;
        }
        this.Write(ex.ToString());
    }
    #endregion
}
public class LogDiagnostics : IDisposable
{
    #region Variable Declarations
    /// <summary>
    /// Writes the output log
    /// </summary>
    private StreamWriter _log = null;
    #endregion

    #region Constructors, Destructors & Disposal
    /// <summary>
    /// Creates a log file that doesn't write to log
    /// </summary>    
    public LogDiagnostics()
    {
    }
    /// <summary>
    /// Default constructor
    /// </summary>
    /// <param name="botName">The name of the bot</param>
    public LogDiagnostics(string botName)
    {
#if VISUAL_STUDIO_2012
        string filename = string.Format("{0}-{1:yyyy-MM-dd HH.mm.ss.ffff}-Diagnostics.csv", botName, DateTime.Now);
        foreach (char c in Path.GetInvalidFileNameChars())
        {
            filename = filename.Replace(c.ToString(), "");
        }
        _log = new StreamWriter(filename);
#endif
    }
    /// <summary>
    /// Closes the log
    /// </summary>
    public void Dispose()
    {
        StreamWriter old = _log;
        _log = null;
        if (old != null)
        {
            old.Flush();
            old.Close();
            old.Dispose();
        }
    }
    #endregion

    #region Public Methods
    public void Flush()
    {
        if (_log != null)
        {
            this._log.Flush();
        }
    }
    /// <summary>
    /// Writes text to the log
    /// </summary>
    /// <param name="text"></param>
    public void Write(int gameTick,
        int pieceIndex,
        int sectionIndex,
        ControlSystems.TrackTypes trackType,
        double throttle,
        double velocity,
        double nextPredictedVelocity,
        double distanceToNextSection,
        double maxTurnVelocity,
        double angle,
        double nextPredictedAngle,
        double maxAnticipatedAngle,
        ControlSystems.Diagnostics.DriftAngles drift,
        double turnRadius,
        double turnAngle,
        bool crash)
    {
        if (_log == null)
        {
            //Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}", gameTick, trackType, throttle, velocity.ToString("0.000", CultureInfo.InvariantCulture), distanceToNextSection.ToString("0.000", CultureInfo.InvariantCulture), maxTurnVelocity.ToString("0.000", CultureInfo.InvariantCulture), angle.ToString("0.000", CultureInfo.InvariantCulture), crash, avgAcceleration, avgDeceleration);
            return;
        }
        //if (trackType == ControlSystems.TrackTypes.Straight)
        //{
        //    return;
        //}
        if (_log.BaseStream.Length == 0)
        {
            _log.WriteLine("Tick, Friction Coefficient, Piece, Section, TrackType, Throttle, Velocity, Next Velocity, Distance To Next Section, Max Turn Velocity, Angle, Next Angle, Max Angle, Drift, Turn Radius, Turn Angle, Crash");
        }
        _log.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16}", gameTick, ControlSystems.Diagnostics.FrictionCoefficient, pieceIndex, sectionIndex, trackType, throttle, velocity.ToString(CultureInfo.InvariantCulture), nextPredictedVelocity.ToString(CultureInfo.InvariantCulture), distanceToNextSection.ToString(CultureInfo.InvariantCulture), maxTurnVelocity.ToString(CultureInfo.InvariantCulture), angle.ToString(CultureInfo.InvariantCulture), nextPredictedAngle.ToString(CultureInfo.InvariantCulture), maxAnticipatedAngle.ToString(CultureInfo.InvariantCulture), drift, turnRadius.ToString(CultureInfo.InvariantCulture), turnAngle.ToString(CultureInfo.InvariantCulture), crash);
        //_log.WriteLine("{0:yyyy-MM-dd HH:mm:ss.ffffff tt}:{1}", DateTime.Now, text.Replace("\n", "\\n"));
        _log.Flush();
    }
    #endregion
}
#endregion
#region Control Systems
namespace ControlSystems
{
    #region Constants
    public enum TrackTypes
    {
        Straight,
        RightTurn,
        LeftTurn,
        Unknown
    }
    #endregion
    #region Controller
    public class Controller
    {
        public CurrentPiece CurrentPiece { get; private set; }
        public Diagnostics Diagnostics { get; private set; }
        public Laptimes LapTimes { get; private set; }
        public Bot Bot { get; set; }
        public Controller(Bot bot)
        {
            this.GameTick = -1;
            this.Bot = bot;
            this.CurrentPiece = new CurrentPiece(this);
            this.Diagnostics = new Diagnostics(this);
            this.LapTimes = new Laptimes(this);
        }
        public void SetPosition(CarPositions.Datum datum)
        {
            this.CurrentPiece.Update(datum);
            this.Diagnostics.Update(datum);
        }
        public int GameTick { get; set; }
        private double _throttle = 0d;
        public double Throttle
        {
            get
            {
                return this._throttle;
            }
            set
            {
                this._throttle = value;
                if (this._throttle < 0d)
                {
                    this._throttle = 0d;
                }
                else if (this._throttle > 1.0d)
                {
                    this._throttle = 1.0d;
                }
                if (this.Bot.Controls.CurrentPiece.Current.TrackType != TrackTypes.Straight)
                {
                    this.Bot.Controls.Diagnostics.NextPredictedAngle = Physics.Instance.PredictNextAngle(this.Bot.Controls.Diagnostics.Angle, this.Bot.Controls.Diagnostics.PreviousAngle, this.Bot.Controls.CurrentPiece.CurrentLane.Radius, this.Bot.Controls.Diagnostics.NextPredictedVelocity);
                }
            }
        }
        public void Crash()
        {
            this.CurrentPiece.Crash();
            this.Diagnostics.Crash();
        }
    }
    #endregion
    #region Diagnostics
    public class Diagnostics
    {
        #region Constants
        private class Throttle
        {
            public double Velocity { get; set; }
            public double Acceleration { get; set; }
            public new string ToString()
            {
                return string.Format("Velocity: {0} Acceleration: {1}", Velocity, Acceleration);
            }
        }
        private class TurnAttack
        {
            /// <summary>
            /// The length of the turn
            /// </summary>
            public double TurnLength { get; set; }
            /// <summary>
            /// The speed with which I attacked the turn
            /// </summary>
            public double Speed { get; set; }
        }
        public enum DriftAngles
        {
            Stable,
            GettingBetter,
            GettingWorse
        }
        #endregion

        #region Events Declarations
        #endregion

        #region Variable Declarations
        private bool _initialized = false;
        private ControlSystems.TrackController.SingleSection _previousSingleSection;
        /// <summary>
        /// The current track
        /// </summary>
        private ControlSystems.TrackTypes _currentTrackType = ControlSystems.TrackTypes.Unknown;
        /// <summary>
        /// Stores some diagnostic information about the turns I've attacked
        /// </summary>
        List<TurnAttack> _turnAttacks = new List<TurnAttack>();
        /// <summary>
        /// The total distance that was accounted for in the previous piece
        /// </summary>
        private double _distanceCoveredInPreviousTrackSection = 0d;
        /// <summary>
        /// The previous piece for which distance was measured
        /// </summary>
        private TrackController.TrackLane _previousTrackSection = null;
        /// <summary>
        /// The distance that was measured
        /// </summary>
        private double _distanceMeasured = 0d;
        /// <summary>
        /// The lanes
        /// </summary>
        private Dictionary<int, GameInit.Lane> _lanes = new Dictionary<int, GameInit.Lane>();
        /// <summary>
        /// Keeps track of all the speeds to get an average speed
        /// </summary>
        private List<double> _averageSpeedCount = new List<double>();
        #endregion

        #region Constructors, Destructors & Disposal
        /// <summary>
        /// Initializes the static variables
        /// </summary>
        static Diagnostics()
        {
            //FrictionCoefficient = 0.374d;
            FrictionCoefficient = 0.325d;
            //FrictionCoefficient = 0.001d;
            //I am expecting to hit my first turn at an angle of 0.190913830859396
            //FrictionCoefficient = double.MaxValue;
        }
        /// <summary>
        /// Initializes the diagnostics
        /// </summary>
        /// <param name="pieces"></param>
        /// <param name="lanes"></param>
        public Diagnostics(Controller controller)
        {
            this.Controller = controller;
            this.FrameRate = 60;
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// The previous throttle value
        /// </summary>
        public double PreviousThrottle { get; set; }
        /// <summary>
        /// The controller
        /// </summary>
        public Controller Controller { get; private set; }
        /// <summary>
        /// Determines how easy/hard it is to lose traction
        /// </summary>
        public static double FrictionCoefficient { get; set; }
        /// <summary>
        /// The distance covered on the track
        /// </summary>
        public double Odometer { get; set; }
        /// <summary>
        /// The number of ticks that happen per second
        /// </summary>
        public int FrameRate { get; private set; }
        /// <summary>
        /// The angle that the car has drifted
        /// </summary>
        public double Angle { get; private set; }
        /// <summary>
        /// The previous angle value
        /// </summary>
        public double PreviousAngle { get; set; }
        /// <summary>
        /// The next predicted angle
        /// </summary>
        public double NextPredictedAngle { get; set; }
        /// <summary>
        /// Defines the current status of the drift angle
        /// </summary>
        public DriftAngles Drift { get; set; }
        /// <summary>
        /// The actual value of the drift
        /// </summary>
        public double DriftValue { get; set; }
        /// <summary>
        /// The current speed of the car
        /// </summary>
        public double CurrentVelocity { get; private set; }
        /// <summary>
        /// The next velocity that was predicted
        /// </summary>
        public double NextPredictedVelocity { get; set; }
        /// <summary>
        /// The average velocity
        /// </summary>
        public double AverageVelocity { get; private set; }
        /// <summary>
        /// The acceleration/deceleration
        /// </summary>
        public double Acceleration { get; set; }
        /// <summary>
        /// The previous acceleration
        /// </summary>
        public double PreviousAcceleration { get; set; }
        /// <summary>
        /// The maximum speed that I can attack the next turn at
        /// </summary>
        public double MaxTurnAttackSpeed { get; set; }
        /// <summary>
        /// The estimated lap time
        /// </summary>
        public TimeSpan EstimatedLapTime { get; set; }
        #endregion

        #region Public Methods
        /// <summary>
        /// Recalculates the turn maximum attack speed for the section
        /// </summary>
        /// <param name="section"></param>
        public double CalculateNextTurnMaximumAttackSpeed(ControlSystems.TrackController.TrackSection section)
        {
            int index = this.Controller.CurrentPiece.CurrentLane.Index;
            if ((section.NextSection.IsSwitch == true) && (section.NextSection.Triggered == Messages.SwitchLane.Direction.Right))
            {
                index++;
                if (index >= section.Lanes.Count)
                {
                    index = section.Lanes.Count - 1;
                }
            }
            else if ((section.NextSection.IsSwitch == true) && (section.NextSection.Triggered == Messages.SwitchLane.Direction.Left))
            {
                index--;
                if (index < 0)
                {
                    index = 0;
                }
            }
            double d = section.NextSection.Lanes[index].SingleSection.MaximumAttackSpeed;
            if (d == 0)
            {
                d = Math.Sqrt(FrictionCoefficient * section.Lanes[index].Radius) * 1.10;
            }
            if (section.NextSection.Lanes[index].SingleSection.MaximumSpeedBeforeSlipping > d)
            {
                d = section.NextSection.Lanes[index].SingleSection.MaximumSpeedBeforeSlipping;
            }
            return d;
        }
        public double CalculateNextTurnMaximumAttackSpeed(ControlSystems.TrackController.TrackLane lane)
        {
            double d = lane.SingleSection.MaximumAttackSpeed;
            if (d == 0)
            {
                d = Math.Sqrt(FrictionCoefficient * lane.Radius) * 1.10;
            }
            if (lane.SingleSection.MaximumSpeedBeforeSlipping > d)
            {
                d = lane.SingleSection.MaximumSpeedBeforeSlipping;
            }
            return d;
        }
        /// <summary>
        /// Resets settings to measure data
        /// </summary>
        public void Crash()
        {
        }
        /// <summary>
        /// Updates the controller
        /// </summary>
        /// <param name="datum"></param>
        /// <param name="gameTime"></param>
        public void Update(CarPositions.Datum datum)
        {
            this.PreviousThrottle = this.Controller.Throttle;
            //reset any previous switch triggers            
            if (this._currentTrackType != this.Controller.CurrentPiece.Current.TrackType)
            {
                this._previousSingleSection = this.Controller.CurrentPiece.CurrentLane.SingleSection;

                if ((this.Controller.CurrentPiece.Current != this.Controller.CurrentPiece.Current.PreviousSection)
                && (this.Controller.CurrentPiece.Current.PreviousSection.IsSwitch == true))
                {
                    this.Controller.CurrentPiece.Current.NextSwitch.Triggered = Messages.SwitchLane.Direction.None;
                }
                if (this.Controller.CurrentPiece.Current.TrackType != ControlSystems.TrackTypes.Straight)
                {
                    if (this.Controller.CurrentPiece.DistanceToNextSection > 120)
                    {
                        TurnAttack attack = new TurnAttack();
                        attack.TurnLength = this.Controller.CurrentPiece.DistanceToNextSection;
                        attack.Speed = this.CurrentVelocity;
                        this._turnAttacks.Add(attack);
                    }
                    else
                    {
                    }
                }
                //if (ControlSystems.ThrottleMeasure.Instance.Completed == true)// && (this.Controller.CurrentPiece.Current.NextSwitch.tr))

                if (this.Controller.Bot.LastSendTick != this.Controller.GameTick)
                {
                    if (this.Controller.CurrentPiece.Current.NextSwitch.StretchDistances[0].Index > this.Controller.CurrentPiece.CurrentLane.Index)
                    {
                        this.Controller.CurrentPiece.Current.NextSwitch.Triggered = Messages.SwitchLane.Direction.Right;
                        this.Controller.Bot.SendMessage(new Messages.SwitchLane(Messages.SwitchLane.Direction.Right));
                    }
                    else if (this.Controller.CurrentPiece.Current.NextSwitch.StretchDistances[0].Index < this.Controller.CurrentPiece.CurrentLane.Index)
                    {
                        this.Controller.CurrentPiece.Current.NextSwitch.Triggered = Messages.SwitchLane.Direction.Left;
                        this.Controller.Bot.SendMessage(new Messages.SwitchLane(Messages.SwitchLane.Direction.Left));
                    }
                }
            }
            this._currentTrackType = this.Controller.CurrentPiece.Current.TrackType;
            this.CalculateDrift(datum.angle);
            this.UpdateOdometer();
            this.CalculateVelocity();
            this.CalculateNextTurnMaximumAttackSpeed();
        }
        /// <summary>
        /// Calculates the coefficient that determines when I'll slide
        /// </summary>
        /// <param name="currentPiece"></param>
        /// <param name="datum"></param>
        public void CalculateFrictionCoefficient(double V, double A, double R)
        {
            A = Math.Abs(A);
            R = Math.Abs(R);
            double B = RadianToDegrees(V / R);
            double Bslip = B - A;
            double Vthresh = DegreeToRadian(Bslip) * R;
            //this is my friction cofficient!
            FrictionCoefficient = (Vthresh * Vthresh) / R;
        }
        #endregion

        #region Internal Methods
        #endregion

        #region Private Methods
        /// <summary>
        /// Helper function to convert degrees to radians
        /// </summary>
        /// <param name="angle"></param>
        /// <returns></returns>
        public double DegreeToRadian(double angle)
        {
            return Math.PI * angle / 180.0d;
        }
        /// <summary>
        /// Helper function to convert radians to degrees
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public double RadianToDegrees(double val)
        {
            return val * 180 / Math.PI;
        }
        /// <summary>
        /// Calculates the maximum speed that I can attack the next turn
        /// </summary>
        private void CalculateNextTurnMaximumAttackSpeed()
        {
            if (this.Controller.GameTick == 163)
            {
            }
            if (FrictionCoefficient >= double.MaxValue)
            {
                this.MaxTurnAttackSpeed = 1.5d;
                return;
            }
            ControlSystems.TrackController.TrackSection nextSection = this.Controller.CurrentPiece.Current;
            while (
                ((nextSection = nextSection.NextSection).Lanes[0].SingleSection.Index == this.Controller.CurrentPiece.Current.Lanes[0].SingleSection.Index)
                ||
                ((nextSection.TreatAsStraight == true) && (this.Controller.CurrentPiece.Current.TrackType == TrackTypes.Straight))
                )
            {
            }
            if (nextSection.TrackType == TrackTypes.Straight)
            {
                return;
            }
            this.MaxTurnAttackSpeed = this.CalculateNextTurnMaximumAttackSpeed(nextSection);
        }
        /// <summary>
        /// Updates the distance traveled
        /// </summary>
        private void UpdateOdometer()
        {
            if (this._previousTrackSection == null)
            {
                this._previousTrackSection = this.Controller.CurrentPiece.CurrentLane;
            }
            if ((this._previousTrackSection != null) && (this._previousTrackSection.Section.Index != this.Controller.CurrentPiece.Current.Index))
            {
                this.Odometer += (this._previousTrackSection.Length - this._distanceCoveredInPreviousTrackSection);
                this._previousTrackSection = this.Controller.CurrentPiece.CurrentLane;
                this._distanceCoveredInPreviousTrackSection = 0d;
            }
            if (_initialized == false)
            {
                _initialized = true;
                this._distanceCoveredInPreviousTrackSection = this.Controller.CurrentPiece.DistanceCovered;
            }
            this.Odometer += (this.Controller.CurrentPiece.DistanceCovered - this._distanceCoveredInPreviousTrackSection);
            this._distanceCoveredInPreviousTrackSection = this.Controller.CurrentPiece.DistanceCovered;
        }
        /// <summary>
        /// Calculates the current speed of the car
        /// </summary>
        /// <param name="datum"></param>
        private void CalculateVelocity()
        {
            double change = this.Odometer - this._distanceMeasured;
            double previousVelocity = this.CurrentVelocity;
            this.CurrentVelocity = change;
            PreviousAcceleration = this.Acceleration;
            this.Acceleration = this.CalculateAcceleration(previousVelocity, this.CurrentVelocity);
            this._averageSpeedCount.Add(this.CurrentVelocity);
            this.AverageVelocity = this._averageSpeedCount.Average();
            this._distanceMeasured = this.Odometer;
        }
        /// <summary>
        /// Calculates the acceleration/deceleration
        /// </summary>
        /// <param name="initialVelocity"></param>
        /// <param name="finalVelocity"></param>
        /// <param name="gameTime"></param>
        /// <returns></returns>
        private double CalculateAcceleration(double initialVelocity, double finalVelocity)
        {
            return (finalVelocity - initialVelocity) / 1;
        }
        /// <summary>
        /// Calculates the drift
        /// </summary>
        /// <param name="angle"></param>
        private void CalculateDrift(double angle)
        {
            this.PreviousAngle = this.Angle;
            this.DriftValue = Math.Abs(Math.Abs(angle) - Math.Abs(this.Angle));
            if (Math.Abs(angle) < Math.Abs(this.Angle))
            {
#warning I'll add this later, it's correct though
                if ((angle > 0) && (this.Controller.CurrentPiece.Current.TrackType == TrackTypes.LeftTurn))
                {
                    //drifting left
                    this.Drift = DriftAngles.GettingWorse;
                }
                else if ((angle < 0) && (this.Controller.CurrentPiece.Current.TrackType == TrackTypes.RightTurn))
                {
                    //drifting right
                    this.Drift = DriftAngles.GettingWorse;
                }
                else
                {
                    this.Drift = DriftAngles.GettingBetter;
                }
                //this.Drift = DriftAngles.GettingBetter;
            }
            else if (Math.Abs(angle) > Math.Abs(this.Angle))
            {
                this.Drift = DriftAngles.GettingWorse;
            }
            else
            {
                this.Drift = DriftAngles.Stable;
            }
            this.Angle = angle;
        }
        #endregion
    }
    #endregion
    #region CurrentPiece
    /// <summary>
    /// The location of my car on the piece
    /// </summary>
    public class CurrentPiece
    {
        #region Constants
        #endregion

        #region Events Declarations
        #endregion

        #region Variable Declarations
        #endregion

        #region Constructors, Destructors & Disposal
        /// <summary>
        /// Initializes the CurrentPiece
        /// </summary>
        /// <param name="pieces"></param>
        public CurrentPiece(Controller controller)
        {
            this.Controller = controller;
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// The controller
        /// </summary>
        public Controller Controller { get; private set; }
        /// <summary>
        /// The lane that I am currently driving in
        /// </summary>
        public ControlSystems.TrackController.TrackLane CurrentLane { get; set; }
        /// <summary>
        /// The piece that I am currently on
        /// </summary>
        public ControlSystems.TrackController.TrackSection Current { get; set; }
        /// <summary>
        /// How far have I gone on the current piece
        /// </summary>
        public double PercentageCovered { get; set; }
        /// <summary>
        /// How far have I gone on the current piece
        /// </summary>
        public double DistanceCovered { get; set; }
        /// <summary>
        /// The distance to the next turn
        /// </summary>
        public double DistanceToNextSection { get; set; }
        /// <summary>
        /// The next section type
        /// </summary>
        public TrackTypes NextSectionType { get; set; }
        #endregion

        #region Public Methods
        /// <summary>
        /// Resets settings to measure data
        /// </summary>
        public void Crash()
        {
            this.CurrentLane.SingleSection.MaximumAngle = 0d;
            this.CurrentLane.SingleSection.MaximumSpeedBeforeSlipping = 0d;
        }
        /// <summary>
        /// Updates the current piece information
        /// </summary>
        /// <param name="datum"></param>
        public void Update(CarPositions.Datum datum)
        {
            this.Current = ControlSystems.TrackController.Instance.Sections[datum.piecePosition.pieceIndex];
            //get the lane that I am currently in
            TrackController.TrackLane oldLane = this.CurrentLane;
            this.CurrentLane = this.Current.Lanes.FirstOrDefault(m => m.Index == datum.piecePosition.lane.startLaneIndex);
            if ((oldLane != null) && (this.CurrentLane.SingleSection.Index != oldLane.SingleSection.Index))
            {
                if (oldLane.SingleSection.MaximumAchievedSpeed > oldLane.SingleSection.MaximumAttackSpeed)
                {
                    oldLane.SingleSection.MaximumAttackSpeed = oldLane.SingleSection.MaximumAchievedSpeed;
                }
            }
            this.HowFarHaveITraveledOnThisPiece(datum);
            //calculate how far I have traveled along the current piece 
            this.MeasureDistanceToNextSection();
        }
        #endregion

        #region Internal Methods
        #endregion

        #region Private Methods
        /// <summary>
        /// Calculates how far I have traveled on the lane that I am on
        /// </summary>
        /// <param name="lane"></param>
        /// <param name="datum"></param>
        private void HowFarHaveITraveledOnThisPiece(CarPositions.Datum datum)
        {
            this.DistanceCovered = datum.piecePosition.inPieceDistance;
            if (this.DistanceCovered > this.CurrentLane.Length)
            {
                this.DistanceCovered = this.CurrentLane.Length;
            }
            this.PercentageCovered = datum.piecePosition.inPieceDistance / this.CurrentLane.Length;
            if (this.PercentageCovered >= 1.0d)
            {
                this.PercentageCovered = 1.0d;
            }
            if (this.Current.TrackType == TrackTypes.RightTurn)
            {
                //right turn                           
                //float length = (float)(currentPiece.angle.Value / 360f * 2 * Math.PI * lane.distanceFromCenter);
                //double length = (Math.Abs(this.Current.angle.Value) / 360d) * (2 * Math.PI * Math.Abs(this.Current.radius.Value + lane.distanceFromCenter));                                
                //this.PercentageCovered = datum.piecePosition.inPieceDistance / this.Current.radius.Value;
            }
            else if (this.Current.TrackType == TrackTypes.LeftTurn)
            {
                //left turn                
                //float length = (float)(currentPiece.angle.Value / 360f * 2 * Math.PI * lane.distanceFromCenter);
                //double length = (Math.Abs(this.Current.angle.Value) / 360d) * (2 * Math.PI * Math.Abs(this.Current.radius.Value + lane.distanceFromCenter));                
                //this.PercentageCovered = datum.piecePosition.inPieceDistance / Math.Abs(this.Current.radius.Value);
            }
            else
            {
                //this is a straight piece
                //this.PercentageCovered = datum.piecePosition.inPieceDistance / this.CurrentLane.Length;
            }
        }
        /// <summary>
        /// Measures the distance to the next turn
        /// </summary>
        /// <param name="lane"></param>
        /// <param name="datum"></param>
        private void MeasureDistanceToNextSection()
        {
            this.DistanceToNextSection = 0;
            TrackController.TrackSection section = this.Current;
            TrackController.TrackSection startSection = section;
            if (Physics.Instance.IsTurboActive == true)
            {
                do
                {
                    this.DistanceToNextSection += section.Lanes[this.CurrentLane.Index].Length;
                } while (((section = section.NextSection).TrackType == this.Current.TrackType) && (section.Index != startSection.Index));
            }
            else
            {
                do
                {
                    this.DistanceToNextSection += section.Lanes[this.CurrentLane.Index].Length;
                } while (((section = section.NextSection).Lanes[this.CurrentLane.Index].SingleSection.Index == this.CurrentLane.SingleSection.Index) && (section.Index != startSection.Index));
            }
            this.DistanceToNextSection -= this.DistanceCovered;
            this.NextSectionType = section.TrackType;
        }
        #endregion
    }
    #endregion
    #region Physics
    /// <summary>
    /// Performs physics calculations in game
    /// </summary>
    public class Physics
    {
        #region Constants
        #endregion

        #region Events Declarations
        #endregion

        #region Variable Declarations
        /// <summary>
        /// Stores the information about the next turbo
        /// </summary>
        private TurboAvailable.Data _nextTurbo = null;
        /// <summary>
        /// Registers the initial speeds while going at a constant rate
        /// </summary>
        private List<double> _initialSpeeds = new List<double>();
        /// <summary>
        /// The tick after which the turbo is no active anymore
        /// </summary>
        private int _turboExpiry = 0;
        /// <summary>
        /// Determines if the engine is in turbo mode
        /// </summary>
        private double _turboMultiplier = 1d;
        /// <summary>
        /// Am I in a crash waiting to respawn
        /// </summary>
        private bool _waitingForRespawn = false;
        #endregion

        #region Constructors, Destructors & Disposal
        /// <summary>
        /// Initializes the singleton instance
        /// </summary>
        static Physics()
        {
            Instance = new Physics();
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// The singleton instance
        /// </summary>
        public static Physics Instance { get; set; }
        /// <summary>
        /// The drag constant due to aerodynamics or whatever else. 
        /// It's due to cheese. The drag constant is due to cheese.
        /// </summary>
        public double DragConstant { get; private set; }
        /// <summary>
        /// The mass of the car
        /// </summary>
        public double MassOfCar { get; private set; }
        /// <summary>
        /// Has the physics been calculated?
        /// </summary>
        public bool Completed { get; private set; }
        /// <summary>
        /// Is there a turbo available?
        /// </summary>
        public bool TurboAvailable
        {
            get
            {
                return this._nextTurbo != null;
            }
        }
        /// <summary>
        /// Is the turbo currently active?
        /// </summary>
        public bool IsTurboActive
        {
            get
            {
                return this._turboMultiplier > 1.0d;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Calculates how many ticks I can keep the throttle on before I have to turn it off!
        /// </summary>
        /// <returns></returns>
        public bool CanIKeepThrottleWithTurbo(Bot bot)
        {
            double howFarAmIfromTheNextTurn = bot.Controls.CurrentPiece.CurrentLane.Length - bot.Controls.CurrentPiece.DistanceCovered;
            ControlSystems.TrackController.TrackSection nextSection = bot.Controls.CurrentPiece.Current.NextSection;
            while (nextSection.TrackType == bot.Controls.CurrentPiece.Current.TrackType)
            {
                nextSection = nextSection.NextSection;
                var lane = nextSection.Lanes.FirstOrDefault(m => m.Index == bot.Controls.CurrentPiece.CurrentLane.Index);
                if (lane != null)
                {
                    howFarAmIfromTheNextTurn += lane.Length;
                }
            }
            double nextSectionLength = 0d;
            double radius = 0d;
            while (nextSection.TrackType != TrackTypes.Straight)
            {
                var lane = nextSection.Lanes.FirstOrDefault(m => m.Index == bot.Controls.CurrentPiece.CurrentLane.Index);
                if (lane != null)
                {
                    nextSectionLength += lane.Length;
                    if (Math.Abs(lane.Radius) > radius)
                    {
                        radius = Math.Abs(lane.Radius);
                    }
                }
                nextSection = nextSection.NextSection;
            }
            double speed = bot.Controls.Diagnostics.CurrentVelocity;
            double distanceCovered = 0;
            while (distanceCovered < howFarAmIfromTheNextTurn)
            {
                distanceCovered += speed;
                speed = CalculateSpeed(speed, 1, 1.0d);
            }
            double maxSpeed = 0d;
            if (bot.CalculateMaxAngle(radius, out maxSpeed, nextSectionLength) > 150)
            {
                return false;
            }
            return true;
        }
        /// <summary>
        /// Registers the velocity
        /// </summary>
        /// <param name="velocity"></param>
        public void RegisterVelocity(double velocity, double throttle)
        {
            throttle = ApplyTurbo(throttle);
            this._initialSpeeds.Add(velocity);
            if (this._initialSpeeds.Count >= 3)
            {
                this.Completed = true;
                this.CalculateDragConstant(throttle);
                this.CalculateMassOfCar(throttle);
                //double terminalVelocity = TerminalVelocity(throttle);
                //double speedIn10 = CalculateSpeed(velocity, 10, throttle);
                //double ticksRequired = CalculateRequiredTicks(velocity, 1.61684327517927, throttle);
                //double distanceTraveled = CalculateDistanceTraveled(0.14, 10, throttle);

                //expecting 0.999
                //double t = CalculateThrottleRequired(6.30106098381128, 6.37483976413506);

                //expecting 0.999
                //double t = CalculateThrottleRequired(6.0428501, 6.0257585);
            }
        }
        /// <summary>
        /// Returns the maximum speed that the car can reach at the throttle level
        /// </summary>
        /// <param name="throttle"></param>
        /// <returns></returns>
        public double TerminalVelocity(double throttle)
        {
            throttle = ApplyTurbo(throttle);
            //v = h/k
            return throttle / this.DragConstant;
        }
        /// <summary>
        /// Calculates the speed after the number of ticks have elapsed
        /// </summary>
        /// <param name="currentSpeed"></param>
        /// <param name="numberOfTicks"></param>
        /// <param name="throttle"></param>
        /// <returns></returns>
        public double CalculateSpeed(double currentSpeed, double numberOfTicks, double throttle)
        {
            throttle = ApplyTurbo(throttle);
            //v(t) = (v(0) - (h/k) ) * e^ ( ( - k * t ) / m ) + ( h/k )
            return (currentSpeed - (throttle / this.DragConstant)) * Math.Pow(Math.E, ((-this.DragConstant * numberOfTicks) / this.MassOfCar)) + (throttle / this.DragConstant);
        }
        /// <summary>
        /// Calculates the number of ticks required to reach a speed
        /// </summary>
        /// <param name="currentSpeed"></param>
        /// <param name="requiredSpeed"></param>
        /// <param name="throttle"></param>
        /// <returns></returns>
        public int CalculateRequiredTicks(double currentSpeed, double requiredSpeed, double throttle)
        {
            throttle = ApplyTurbo(throttle);
            //t = ( ln ( (v - ( h/k ) )/(v(0) - ( h/k ) ) ) * m ) / ( -k )
            double d = (Math.Log((requiredSpeed - (throttle / this.DragConstant)) / (currentSpeed - (throttle / this.DragConstant))) * this.MassOfCar) / (-this.DragConstant);
            if (d <= 0)
            {
                return 1;
            }
            else
            {
                return (int)Math.Round(d, 0);
            }
        }
        /// <summary>
        /// Calculates the distance traveled
        /// </summary>
        /// <param name="currentSpeed"></param>
        /// <param name="numberOfTicks"></param>
        /// <param name="throttle"></param>
        /// <returns></returns>
        public double CalculateDistanceTraveled(double currentSpeed, int numberOfTicks, double throttle)
        {
            throttle = ApplyTurbo(throttle);
            //d(t) = ( m/k ) * ( v(0) - ( h/k ) ) * ( 1.0 - e^ ( ( -k*t ) / m ) ) + ( h/k ) * t + d(0)
            double d = (this.MassOfCar / this.DragConstant) * (currentSpeed - (throttle / this.DragConstant)) * (1.0 - Math.Pow(Math.E, ((-this.DragConstant * numberOfTicks) / this.MassOfCar))) + (throttle / this.DragConstant) * numberOfTicks + 0;
            return d + currentSpeed;
        }
        /// <summary>
        /// Calculates the amount of throttle that is required to reach a speed
        /// </summary>
        /// <param name="currentSpeed"></param>
        /// <param name="requiredSpeed"></param>
        /// <returns></returns>
        public double CalculateThrottleRequired(double currentSpeed, double requiredSpeed)
        {
            double throttle = 0.0d;
            double newVelocity = currentSpeed;

            if (currentSpeed > requiredSpeed)
            {
                throttle = 1.0d;
                do
                {
                    throttle -= 0.0001d;
                    newVelocity = ControlSystems.Physics.Instance.CalculateSpeed(newVelocity, 1, throttle);
                } while ((newVelocity - requiredSpeed > 0.0001d) && (throttle > 0.0d));
            }
            else
            {
                do
                {
                    throttle += 0.0001d;
                    newVelocity = ControlSystems.Physics.Instance.CalculateSpeed(newVelocity, 1, throttle);
                } while ((newVelocity - requiredSpeed < 0.0001d) && (throttle < 1.0d));
            }

            if ((currentSpeed > requiredSpeed) && (throttle >= 1.0))
            {
                return 0d;
            }
            if (throttle < 0)
            {
                return 0d;
            }
            if (throttle > 1.0d)
            {
                return 1.0d;
            }
            return throttle;
        }
        /// <summary>
        /// Enables the turbo!! Yeeeeeeeeehaw.
        /// </summary>
        /// <param name="gameTick"></param>
        public void EnableTurbo(Bot bot, int gameTick)
        {
            if ((this._turboMultiplier == 1d) && (this._nextTurbo != null))
            {
                this._turboExpiry = gameTick + this._nextTurbo.turboDurationTicks;
                this._turboMultiplier = this._nextTurbo.turboFactor;
                this._nextTurbo = null;
                bot.SendMessage(new Messages.Turbo());
            }
        }
        /// <summary>
        /// Updates the turbo
        /// </summary>
        /// <param name="gameTick"></param>
        public void UpdateTurbo(int gameTick)
        {
            if (this._turboExpiry < gameTick)
            {
                this._turboExpiry = 0;
                this._turboMultiplier = 1d;
            }
        }
        /// <summary>
        /// Configure the availability of the turbo
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        public void RegisterTurbo(int duration, double multiplier)
        {
            if (this._waitingForRespawn == false)
            {
                //turbo's will only stack if you are currently using your turbo            
                this._nextTurbo = new TurboAvailable.Data();
                this._nextTurbo.turboDurationTicks = duration;
                this._nextTurbo.turboFactor = multiplier;
            }
        }
        /// <summary>
        /// Signals that I am waiting to respawn
        /// </summary>
        public void Crash()
        {
            this._waitingForRespawn = true;
        }
        /// <summary>
        /// Spawned
        /// </summary>
        public void Respawn()
        {
            this._waitingForRespawn = false;
        }
        /// <summary>
        /// Predicts the next angle
        /// </summary>
        /// <param name="currentAngle"></param>
        /// <param name="radius"></param>
        /// <param name="nextVelocity"></param>
        /// <returns></returns>
        public double PredictNextAngle(double currentAngle, double previousAngle, double radius, double nextVelocity)
        {
            //double maximumCalculatedTurnVelocity = Math.Sqrt(ControlSystems.Diagnostics.FrictionCoefficient * radius);
            ////double howMuchWillMyCurrentVelocityAddToTheAngle = nextVelocity / maximumCalculatedTurnVelocity;            
            //double howMuchWillMyCurrentVelocityAddToTheAngle = Math.Sqrt(nextVelocity * maximumCalculatedTurnVelocity * Diagnostics.FrictionCoefficient) / (3.3d / Diagnostics.FrictionCoefficient);
            //if (nextVelocity > maximumCalculatedTurnVelocity)
            //{
            //    return Math.Abs(currentAngle) + howMuchWillMyCurrentVelocityAddToTheAngle + (int)(currentAngle / (3.3d / Diagnostics.FrictionCoefficient));
            //}
            //else
            //{
            //    return Math.Abs(currentAngle) - howMuchWillMyCurrentVelocityAddToTheAngle;// -(int)(currentAngle / 10);
            //}
            double angularVelocity = currentAngle - previousAngle;
            double linearVelociry = nextVelocity / radius;
            double pointVelocity = 0d;
            //if (angularVelocity < 0)
            //{
            //    pointVelocity = -linearVelociry - angularVelocity;
            //    return pointVelocity - currentAngle;
            //}
            //else
            //{
            pointVelocity = linearVelociry + angularVelocity;
            return pointVelocity + currentAngle;
            //}            
        }
        #endregion

        #region Internal Methods
        #endregion

        #region Private Methods
        /// <summary>
        /// Applies the turbo to the throttle
        /// </summary>
        /// <param name="throttle"></param>
        /// <returns></returns>
        private double ApplyTurbo(double throttle)
        {
            return throttle * this._turboMultiplier;
        }
        /// <summary>
        /// Calculates the drag force
        /// </summary>
        /// <param name="throttle"></param>
        private void CalculateDragConstant(double throttle)
        {
            //k = ( v(1) - ( v(2) - v(1) ) ) / v(1)2 * h;
            this.DragConstant = (this._initialSpeeds[0] - (this._initialSpeeds[1] - this._initialSpeeds[0])) / Math.Pow(this._initialSpeeds[0], 2) * throttle;
        }
        /// <summary>
        /// Calculates the mass of the car
        /// </summary>
        /// <param name="throttle"></param>
        private void CalculateMassOfCar(double throttle)
        {
            //m = 1.0 / ( ln( ( v(3) - ( h / k ) ) / ( v(2) - ( h / k ) ) ) / ( -k ) )
            double problem = (this._initialSpeeds[2] - (throttle / this.DragConstant)) / (this._initialSpeeds[1] - (throttle / this.DragConstant));
            if (problem < 0)
            {
                //    problem = 0.99d;
            }
            this.MassOfCar = 1.0 / (Math.Log(problem) / (-this.DragConstant));
        }
        #endregion
    }
    #endregion
    #region Tracks
    /// <summary>
    /// Performs track measurements
    /// </summary>
    public class TrackController
    {
        #region Classes
        /// <summary>
        /// Defines a single continuous section of road of which all properties are the same
        /// </summary>
        public class SingleSection
        {
            private static int _index = 0;
            public double MaximumAngle { get; set; }
            private double d;
            public double MaximumAttackSpeed { get; set; }
            public double MaximumAchievedSpeed { get; set; }
            public double MaximumSpeedBeforeSlipping
            {
                get
                {
                    return d;
                }
                set
                {
                    d = value;
                    if (d == 7.2107745524626807)
                    {
                    }
                }
            }
            public bool AllowTurbo { get; set; }
            //7.2107745524626807
            //public double MaximumAchievedSpeed { get; set; }
            public int Index { get; set; }
            public SingleSection()
            {
                this.Index = _index++;
                this.AllowTurbo = true;
            }
            public static SingleSection LongestStraight { get; set; }
            public new string ToString()
            {
                return string.Format("Index: {0}", this.Index);
            }

            public double TotalLength { get; set; }

            public double StretchLength { get; set; }
        }
        /// <summary>
        /// Defines a lane in a section of track
        /// </summary>
        public class TrackLane
        {
            #region Constants
            #endregion

            #region Events Declarations
            #endregion

            #region Variable Declarations
            #endregion

            #region Constructors, Destructors & Disposal
            /// <summary>
            /// Initializes the lane
            /// </summary>
            /// <param name="index"></param>
            /// <param name="length"></param>
            public TrackLane(TrackSection section, int index, double length)
            {
                this.Section = section;
                this.Length = length;
                this.Index = index;
            }
            #endregion

            #region Public Properties
            public SingleSection SingleSection { get; set; }
            /// <summary>
            /// The section that this lane belongs to
            /// </summary>
            public TrackSection Section { get; private set; }
            /// <summary>
            /// The length of this piece of track
            /// </summary>
            public double Length { get; private set; }
            /// <summary>
            /// The total length of the track if this lane is followed
            /// </summary>
            public double TrackLength { get; internal set; }
            /// <summary>
            /// The index of the lane
            /// </summary>
            public int Index { get; private set; }
            /// <summary>
            /// The radius of this lane
            /// </summary>
            public double Radius { get; internal set; }
            #endregion

            #region Public Methods
            /// <summary>
            /// Debug friendly string
            /// </summary>
            /// <returns></returns>
            public new string ToString()
            {
                return string.Format("Index:{0} Length:{1} Stretch length:{2} Track Length:{3}", this.Index, this.Length, this.SingleSection.TotalLength, this.TrackLength.ToString("###,##0.000"));
            }
            #endregion

            #region Internal Methods
            ///// <summary>
            ///// Adds to the common length
            ///// </summary>
            ///// <param name="length"></param>
            //internal void AddToStretchLength(double length)
            //{
            //    this.StretchLength += length;
            //}
            #endregion

            #region Private Methods
            #endregion
        }
        /// <summary>
        /// Defines a section of the track
        /// </summary>
        public class TrackSection
        {
            #region Constants
            #endregion

            #region Events Declarations
            #endregion

            #region Variable Declarations
            /// <summary>
            /// The length of the section, only for straight sections
            /// </summary>
            private double _length;
            #endregion

            #region Constructors, Destructors & Disposal
            /// <summary>
            /// Creates a new track section
            /// </summary>
            /// <param name="piece"></param>
            /// <param name="lanes"></param>
            public TrackSection(Piece piece, int index, List<GameInit.Lane> lanes)
            {
                this.DetermineTrackPieceType(piece);
                this.Index = index;
                this._length = piece.length;
                this.IsSwitch = piece.@switch != null && piece.@switch == true;
                this.Radius = piece.radius == null ? 0 : piece.radius.Value;
                this.Angle = piece.angle == null ? 0 : piece.angle.Value;
                this.Radius = piece.radius == null ? 0 : piece.radius.Value;
                this.InitializeLanes(piece, lanes);
            }
            #endregion

            #region Public Properties
            private bool? _treatAsStraight = null;
            /// <summary>
            /// Should I treat this section as a straight
            /// </summary>
            public bool TreatAsStraight
            {
                get
                {                    
                    if (this._treatAsStraight == null)
                    {
                        if (this.TrackType == TrackTypes.Straight)
                        {
                            this._treatAsStraight = true;
                            return true;
                        }
                        //TrackSection previous = this.PreviousSection;
                        //while ((previous.Radius == this.Radius) && (previous.Angle == this.Angle))
                        //{
                        //    previous = previous.PreviousSection;
                        //}
                        //TrackSection next = this.NextSection;
                        //while ((next.Radius == this.Radius) && (next.Angle == this.Angle))
                        //{
                        //    next = next.NextSection;
                        //}
                        //this._treatAsStraight = (Math.Abs(this.Angle) < 30) &&
                        //    (Math.Abs(this.Radius) < 150) &&
                        //    (previous.TrackType == TrackTypes.Straight) &&
                        //    (next.TrackType == TrackTypes.Straight);
                        this._treatAsStraight = (Math.Abs(this.Angle) < 30) &&                            
                            (this.PreviousSection.TrackType == TrackTypes.Straight) &&
                            (this.NextSection.TrackType == TrackTypes.Straight);
                    }
                    return this._treatAsStraight.Value;
                }
            }
            /// <summary>
            /// Determines if the bot triggered the switching action here
            /// </summary>
            public Messages.SwitchLane.Direction Triggered { get; set; }
            /// <summary>
            /// The lanes in this section
            /// </summary>
            public List<TrackLane> Lanes { get; set; }
            /// <summary>
            /// Orders the stretches by distance
            /// </summary>
            public List<TrackLane> StretchDistances { get; set; }
            /// <summary>
            /// Is this a switch piece
            /// </summary>
            public bool IsSwitch { get; private set; }
            /// <summary>
            /// If this is a turn, what is the radius
            /// </summary>
            public int Radius { get; private set; }
            /// <summary>
            /// If this is a turn, what is the angle of the turn
            /// </summary>
            public double Angle { get; private set; }
            /// <summary>
            /// The type of track
            /// </summary>
            public TrackTypes TrackType { get; private set; }
            /// <summary>
            /// The index of the track
            /// </summary>
            public int Index { get; private set; }
            /// <summary>
            /// The next section
            /// </summary>
            public TrackSection NextSection { get; private set; }
            /// <summary>
            /// The previous track section
            /// </summary>
            public TrackSection PreviousSection { get; private set; }
            /// <summary>
            /// The next trackpiece that is a switch that follows this piece
            /// </summary>
            public TrackSection NextSwitch { get; set; }
            #endregion

            #region Public Methods
            /// <summary>
            /// Debug friendly string
            /// </summary>
            /// <returns></returns>
            public new string ToString()
            {
                return string.Format("Index:{0} Track Type:{1} (Next Switch: {2})", this.Index, this.TrackType, this.NextSwitch == null ? "?" : string.Format("Index:{0} Track Type:{1}", this.NextSwitch.Index, this.NextSwitch.TrackType));
            }
            #endregion

            #region Internal Methods
            /// <summary>
            /// Links this track section to the next section
            /// </summary>
            /// <param name="section"></param>
            internal void SetNextSection(TrackSection section)
            {
                //for (int i = 0; i < this.Lanes.Count; i++)
                //{
                //    if (this.Lanes[i].SingleSection == null)
                //    {
                //        this.Lanes[i].SingleSection = new SingleSection();
                //    }
                //if ((section.TrackType == this.TrackType) || (section.TreatAsStraight == true))
                //{
                //    section.Lanes[i].SingleSection = this.Lanes[i].SingleSection;
                //}
                //}
                this.NextSection = section;
            }
            /// <summary>
            /// Links this track section to the previous section
            /// </summary>
            /// <param name="section"></param>
            internal void SetPreviousSection(TrackSection section)
            {
                this.PreviousSection = section;
            }
            #endregion

            #region Private Methods
            /// <summary>
            /// Initializes the lanes
            /// </summary>
            /// <param name="lanes"></param>
            private void InitializeLanes(Piece piece, List<GameInit.Lane> lanes)
            {
                this.Lanes = new List<TrackLane>();
                foreach (GameInit.Lane lane in lanes)
                {
                    TrackLane trackLane = null;
                    if (this.TrackType == TrackTypes.LeftTurn)
                    {
                        //left is the inside lane, so keep -1 as -1
                        trackLane = new TrackLane(this, lane.index, this.CalculateLength(this, lane.distanceFromCenter));
                        trackLane.Radius = this.Radius + lane.distanceFromCenter;
                    }
                    else if (this.TrackType == TrackTypes.RightTurn)
                    {
                        //right is the inside lane, so invert the distanceFromCenter
                        trackLane = new TrackLane(this, lane.index, this.CalculateLength(this, lane.distanceFromCenter * -1));
                        trackLane.Radius = this.Radius + (lane.distanceFromCenter * -1);
                    }
                    else
                    {
                        trackLane = new TrackLane(this, lane.index, this.CalculateLength(this, 0));
                        trackLane.Radius = this.Radius + (lane.distanceFromCenter * -1);
                    }
                    this.Lanes.Add(trackLane);
                }
                this.Lanes = this.Lanes.OrderBy(x => x.Index).ToList();
            }
            /// <summary>
            /// Determines the type of track piece
            /// </summary>
            /// <param name="piece"></param>
            private void DetermineTrackPieceType(Piece piece)
            {
                if ((piece.angle == null) || (piece.radius == null))
                {
                    this.TrackType = TrackTypes.Straight;
                }
                else if ((piece.angle > 0) && (Math.Abs(piece.radius.Value) > 0))
                {
                    this.TrackType = TrackTypes.RightTurn;
                }
                else if ((piece.angle < 0) && (Math.Abs(piece.radius.Value) > 0))
                {
                    this.TrackType = TrackTypes.LeftTurn;
                }
                else
                {
                    this.TrackType = TrackTypes.Straight;
                }
            }
            /// <summary>
            /// Calculates the length of the piece
            /// </summary>
            /// <param name="piece"></param>
            private double CalculateLength(TrackSection section, double distanceOffset)
            {
                switch (section.TrackType)
                {
                    case TrackTypes.Straight:
                        {
                            return section._length;
                        }
                    case TrackTypes.RightTurn:
                    case TrackTypes.LeftTurn:
                        {
                            return (Math.Abs(section.Angle) / 360d) * (2 * Math.PI * (Math.Abs(section.Radius) + distanceOffset));
                        }
                }
                return 0d;
            }
            #endregion
        }
        #endregion

        #region Events Declarations
        #endregion

        #region Variable Declarations
        #endregion

        #region Constructors, Destructors & Disposal
        /// <summary>
        /// Initializes the singleton instance
        /// </summary>
        static TrackController()
        {
            Instance = new TrackController();
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// The singleton instance
        /// </summary>
        public static TrackController Instance { get; private set; }
        /// <summary>
        /// The name of the track
        /// </summary>
        public string TrackName { get; set; }
        /// <summary>
        /// The track sections
        /// </summary>
        public List<TrackSection> Sections { get; set; }
        #endregion

        #region Public Methods
        /// <summary>
        /// Initializes the track
        /// </summary>
        /// <param name="init"></param>
        public void Initialize(GameInit.GameInit init)
        {
            if (this.TrackName == init.data.race.track.name)
            {
                return;
            }
            this.TrackName = init.data.race.track.name;
            this.Sections = new List<TrackSection>();
            for (int i = 0; i < init.data.race.track.pieces.Count; i++)
            {
                TrackSection section = new TrackSection(init.data.race.track.pieces[i], i, init.data.race.track.lanes);
                Sections.Add(section);
            }
            this.LinkSections();
            //foreach (TrackSection section in this.Sections)
            //{
            //    foreach (TrackLane lane in section.Lanes)
            //    {                   
            //        lane.SingleSection.TotalLength += lane.Length;
            //    }
            //}
            foreach (TrackSection section in this.Sections)
            {
                this.CalculateStretchLength(section);
                this.CalculateTrackLength(section);
                this.DetermineShortestPaths(section);
            }
            foreach (var section in this.Sections)
            {
                if (section.Angle != 0)
                {
                    foreach (var lane in section.Lanes)
                    {
                        lane.SingleSection.AllowTurbo = false;
                    }
                }
            }
            foreach (var section in this.Sections)
            {
                foreach (var lane in section.Lanes)
                {
                    if (
                        ((SingleSection.LongestStraight == null) || (lane.SingleSection.TotalLength > SingleSection.LongestStraight.TotalLength))
                        && (lane.SingleSection.AllowTurbo == true)
                        )
                    {
                        SingleSection.LongestStraight = lane.SingleSection;
                    }
                }
            }
        }
        #endregion

        #region Internal Methods
        #endregion

        #region Private Methods
        /// <summary>
        /// Links all the track sections together so that they form a linked-list
        /// </summary>
        private void LinkSections()
        {
            for (int i = 0; i < this.Sections.Count - 1; i++)
            {
                this.Sections[i].SetNextSection(this.Sections[i + 1]);
            }
            this.Sections.Last().SetNextSection(this.Sections.First());
            for (int i = 1; i < this.Sections.Count; i++)
            {
                this.Sections[i].SetPreviousSection(this.Sections[i - 1]);
            }
            this.Sections.First().SetPreviousSection(this.Sections.Last());

            foreach (var section in this.Sections)
            {
                LinkSingleSections(section);
            }
        }
        /// <summary>
        /// Links all the single sections together
        /// </summary>
        /// <param name="section"></param>
        private void LinkSingleSections(TrackSection section)
        {            
            for (int i = 0; i < section.Lanes.Count; i++)
            {
                if (section.Lanes[i].SingleSection == null)
                {
                    section.Lanes[i].SingleSection = new SingleSection();                  
                }
            }
            if (
                ((section.TrackType != TrackTypes.Straight) && (section.TrackType == section.NextSection.TrackType) && (section.Radius == section.NextSection.Radius))
                || ((section.TrackType == TrackTypes.Straight) && (section.NextSection.TreatAsStraight == true))
                || ((section.TreatAsStraight == true) && (section.NextSection.TrackType == TrackTypes.Straight))
                )
            {
                if (
                    (Math.Abs(section.Lanes[0].Radius) - Math.Abs(section.NextSection.Lanes[0].Radius) == 0) ||
                    (section.NextSection.TreatAsStraight == true) || (section.TreatAsStraight == true)
                    )
                {
                    bool allow = false;
                    for (int i = 0; i < section.Lanes.Count; i++)
                    {
                        if ((section.NextSection.Lanes[i].SingleSection == null) || (section.NextSection.Lanes[i].SingleSection.Index != section.Lanes[i].SingleSection.Index))
                        {
                            allow = true;
                            section.NextSection.Lanes[i].SingleSection = section.Lanes[i].SingleSection;
                        }
                    }
                    if (allow == true)
                    {
                        LinkSingleSections(section.NextSection);
                    }
                }
            }
            if (
                ((section.TrackType != TrackTypes.Straight) && (section.TrackType == section.PreviousSection.TrackType) && (section.Radius == section.PreviousSection.Radius))
                || ((section.TrackType == TrackTypes.Straight) && (section.PreviousSection.TreatAsStraight == true))
                || ((section.TreatAsStraight == true) && (section.PreviousSection.TrackType == TrackTypes.Straight))
                )
            {
                if (
                    (Math.Abs(section.Lanes[0].Radius) - Math.Abs(section.NextSection.Lanes[0].Radius) == 0) ||
                    (section.PreviousSection.TreatAsStraight == true) || (section.TreatAsStraight == true)
                    )
                {
                    bool allow = false;
                    for (int i = 0; i < section.Lanes.Count; i++)
                    {
                        if ((section.PreviousSection.Lanes[i].SingleSection == null) || (section.PreviousSection.Lanes[i].SingleSection.Index != section.Lanes[i].SingleSection.Index))
                        {
                            allow = true;
                            section.PreviousSection.Lanes[i].SingleSection = section.Lanes[i].SingleSection;
                        }
                    }
                    if (allow == true)
                    {
                        LinkSingleSections(section.PreviousSection);
                    }
                }
            }
            //    for (int i = 0; i < section.Lanes.Count; i++)
            //    {
            //        if (section.PreviousSection.Lanes[i].SingleSection != null)
            //        {
            //            section.Lanes[i].SingleSection = section.PreviousSection.Lanes[i].SingleSection;
            //        }
            //        else
            //        {
            //            if (section.Lanes[i].SingleSection == null)
            //            {
            //                section.Lanes[i].SingleSection = new SingleSection();
            //            }
            //            section.PreviousSection.Lanes[i].SingleSection = section.Lanes[i].SingleSection;
            //        }
            //    }                  
            //}
            //else
            //{
            //    for (int i = 0; i < section.Lanes.Count; i++)
            //    {
            //        if (section.Lanes[i].SingleSection == null)
            //        {
            //            section.Lanes[i].SingleSection = new SingleSection();
            //        }                    
            //    }
            //}
        }
        /// <summary>
        /// Calculates the length for each lane until the next uncommon piece is found
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        private void CalculateStretchLength(TrackSection section)
        {
            for (int index = 0; index < section.Lanes.Count; index++)
            {
                section.Lanes[index].SingleSection.TotalLength += section.Lanes[index].Length;
            }
            //TrackSection nextPiece = section;
            //do
            //{
            //    for (int index = 0; index < section.Lanes.Count; index++)
            //    {
            //        TrackLane lane = nextPiece.Lanes.FirstOrDefault(m => m.Index == section.Index);
            //        if (lane == null)
            //        {
            //            #region find the lane that is nearest to me
            //            lane = nextPiece.Lanes.OrderByDescending(m => m.Index).FirstOrDefault(m => m.Index < index);
            //            if (lane == null)
            //            {
            //                lane = nextPiece.Lanes.OrderBy(m => m.Index).FirstOrDefault(m => m.Index > index);
            //            }
            //            if (lane == null)
            //            {
            //                lane = nextPiece.Lanes.FirstOrDefault();
            //            }
            //            #endregion
            //        }
            //        section.Lanes[index].Sin.
            //        //section.Lanes[index].AddToStretchLength(nextPiece.Lanes[index].Length);
            //    }
            //} while ((nextPiece = nextPiece.NextSection).TrackType == section.TrackType);
        }
        /// <summary>
        /// Calculates the track length for each lane
        /// </summary>
        /// <param name="section"></param>
        private void CalculateTrackLength(TrackSection section)
        {
            TrackSection nextSection = section;
            do
            {
                for (int i = 0; i < section.Lanes.Count; i++)
                {
                    section.Lanes[i].TrackLength += nextSection.Lanes[i].Length;
                }
            } while ((nextSection = nextSection.NextSection).Index != section.Index);
        }
        /// <summary>
        /// Determines the shortest path between two switches
        /// </summary>
        private void DetermineShortestPaths(TrackSection section)
        {
            if (section.IsSwitch == false)
            {
                return;
            }
            section.StretchDistances = new List<TrackLane>();
            //List<double> lanes = new List<double>();
            //for (int i = 0; i < section.Lanes.Count; i++)
            //{
            //    lanes.Add(0d);
            //}
            for (int i = 0; i < section.Lanes.Count; i++)
            {
                TrackLane lane = new TrackLane(section, section.Lanes[i].Index, 0);
                section.StretchDistances.Add(lane);
                lane.SingleSection = new SingleSection();
            }
            TrackSection nextSwitch = section;
            do
            {
                for (int i = 0; i < section.Lanes.Count; i++)
                {
                    for (int l = 0; l < section.StretchDistances.Count; l++)
                    {
                        section.StretchDistances[l].SingleSection.StretchLength += nextSwitch.Lanes[l].Length;
                    }
                }
            } while (((nextSwitch = nextSwitch.NextSection).Index != section.Index) && (nextSwitch.IsSwitch == false));
            section.StretchDistances = section.StretchDistances.OrderBy(m => m.SingleSection.StretchLength).ThenByDescending(m => m.Index).ToList();
            //now tell all the pieces where the next switch is
            TrackSection nextSection = section;
            do
            {
                for (int i = 0; i < section.Lanes.Count; i++)
                {
                    nextSection.NextSwitch = nextSwitch;
                }
            } while (((nextSection = nextSection.NextSection).Index != section.Index) && (nextSection.IsSwitch == false));
        }
        #endregion
    }
    #endregion
    #region LapTimes
    /// <summary>
    /// Keeps track of the lap times
    /// </summary>
    public class Laptimes
    {
        #region Constants
        #endregion

        #region Events Declarations
        #endregion

        #region Variable Declarations
        /// <summary>
        /// Keeps track of the tick that was last used to reset
        /// </summary>
        private int _lastReset = 0;
        #endregion

        #region Constructors, Destructors & Disposal
        /// <summary>
        /// Initializes the Laptimes controller
        /// </summary>
        /// <param name="pieces"></param>
        /// <param name="lanes"></param>
        public Laptimes(Controller controller)
        {
            this.Controller = controller;
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// The controller
        /// </summary>
        public Controller Controller { get; private set; }
        /// <summary>
        /// The current time for this lap
        /// </summary>
        public double Current { get; set; }
        #endregion

        #region Public Methods
        /// <summary>
        /// Resets the timer to ensure that the laps are timed
        /// </summary>
        public void ResetTimer(LapFinished.Data data)
        {
            this._lastReset = this.Controller.GameTick;
        }
        /// <summary>
        /// Calculates the lap time based
        /// </summary>
        public void CalculateCurrentLaptime()
        {
            this.Current = (this.Controller.GameTick - this._lastReset) / 60d;
        }
        #endregion

        #region Internal Methods
        #endregion

        #region Private Methods
        #endregion
    }
    #endregion
    #region OtherCars
    public class OtherCars
    {
        #region Constants
        public class Opponent : Bot
        {
            public Opponent()
            {
                this.Controller = new Controller(this);
            }
            public Controller Controller { get; set; }
        }
        #endregion

        #region Events Declarations
        #endregion

        #region Variable Declarations
        private Dictionary<string, Opponent> _opponents = new Dictionary<string, Opponent>();
        #endregion

        #region Constructors, Destructors & Disposal
        /// <summary>
        /// Initializes the singleton instance
        /// </summary>
        static OtherCars()
        {
            Instance = new OtherCars();
            Instance.InMyLane = new List<Opponent>();
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// The singleton instance
        /// </summary>
        public static OtherCars Instance { get; set; }
        public List<Opponent> InMyLane { get; set; }
        #endregion

        #region Public Methods
        public void Reset()
        {
            this._opponents.Clear();
        }
        /// <summary>
        /// Updates the car position
        /// </summary>
        public void Update(Bot me, CarPositions.Datum datum)
        {
            string key = datum.id.name + "_" + datum.id.color;
            if (this._opponents.ContainsKey(key) == false)
            {
                this._opponents[key] = new Opponent();
            }
            this._opponents[key].Controller.CurrentPiece.Update(datum);
            this._opponents[key].Controller.Diagnostics.Update(datum);
            this.WhosInMyLane(me);
        }
        #endregion

        #region Internal Methods
        #endregion

        #region Private Methods
        private void WhosInMyLane(Bot me)
        {
            foreach (var opponent in this._opponents)
            {
                if (opponent.Value.Controller.CurrentPiece.CurrentLane.Index == me.Controls.CurrentPiece.CurrentLane.Index)
                {
                    this.InMyLane.Add(opponent.Value);
                }
                Opponent inMyWay = this.InMyLane.FindAll(m => m.Controller.CurrentPiece.Current.Index >= me.Controls.CurrentPiece.Current.Index)                    
                    .OrderBy(m => m.Controller.CurrentPiece.Current.Index)
                    .FirstOrDefault(m => m.Controller.CurrentPiece.CurrentLane.SingleSection.Index == me.Controls.CurrentPiece.CurrentLane.SingleSection.Index);
                if (inMyWay != null)
                {
                    if (
                        (Math.Abs(inMyWay.Controller.CurrentPiece.DistanceToNextSection - me.Controls.CurrentPiece.DistanceToNextSection) < 50)
                        && (inMyWay.Controller.CurrentPiece.DistanceToNextSection < me.Controls.CurrentPiece.DistanceToNextSection))
                    {
                        if (me.Controls.CurrentPiece.CurrentLane.Index == 0)
                        {
                            me.SendMessage(new Messages.SwitchLane(Messages.SwitchLane.Direction.Right));
                        }
                        else
                        {
                            me.SendMessage(new Messages.SwitchLane(Messages.SwitchLane.Direction.Left));
                        }                        
                    }
                }
            }
        }
        #endregion
    }
    #endregion
}
#endregion
#region Messages
public class Piece
{
    public double length { get; set; }
    public bool? @switch { get; set; }
    public int? radius { get; set; }
    public double? angle { get; set; }
    public ControlSystems.TrackTypes TrackType { get; set; }
    public int Index { get; set; }
}
namespace Messages
{
    #region Classes
    public class MsgWrapper
    {
        public string msgType;
        public Object data;

        public MsgWrapper(string msgType, Object data)
        {
            this.msgType = msgType;
            this.data = data;
        }
    }
    public abstract class SendMsg
    {
        public string ToJson()
        {
            return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
        }
        protected virtual Object MsgData()
        {
            return this;
        }

        protected abstract string MsgType();
    }
    public class Join : SendMsg
    {
        public string name;
        public string key;
        //public string color;

        public Join(string name, string key)
        {
            this.name = name;
            this.key = key;
        }

        protected override string MsgType()
        {
            return "join";
        }
    }
    public class JoinRace : SendMsg
    {
        public class _botId
        {
            public string name;
            public string key;
        }
        public _botId botId { get; set; }
        public string trackName { get; set; }
        public string password { get; set; }
        public int carCount { get; set; }

        public JoinRace(string name, string key, string trackName, string password, int cars)
        {
            this.botId = new _botId();
            this.botId.name = name;
            this.botId.key = key;
            this.trackName = trackName;
            this.password = password;
            this.carCount = cars;
        }

        protected override string MsgType()
        {
            return "joinRace";
        }
    }
    public class CreateRace : SendMsg
    {
        public class _botId
        {
            public string name;
            public string key;
        }
        public _botId botId { get; set; }
        public string trackName { get; set; }
        //public string password { get; set; }
        public int carCount { get; set; }

        public CreateRace(string name, string key, string trackName, string password, int cars)
        {
            this.botId = new _botId();
            this.botId.name = name;
            this.botId.key = key;
            this.trackName = trackName;
            //this.password = password;
            this.carCount = cars;
        }

        protected override string MsgType()
        {
            return "createRace";
        }
    }
    public class Ping : SendMsg
    {
        protected override string MsgType()
        {
            return "ping";
        }
    }
    public class Throttle : SendMsg
    {
        public double value;

        public Throttle(double value)
        {
            this.value = value;
        }

        protected override Object MsgData()
        {
            return this.value;
        }

        protected override string MsgType()
        {
            return "throttle";
        }
    }
    public class Turbo : SendMsg
    {
        protected override Object MsgData()
        {
            return "Ken sent me!!";
        }
        protected override string MsgType()
        {
            return "turbo";
        }
    }
    public class SwitchLane : SendMsg
    {
        private Direction _direction = Direction.Left;
        public enum Direction
        {
            None,
            Left,
            Right
        }
        public SwitchLane(Direction direction)
        {
            this._direction = direction;
        }
        protected override Object MsgData()
        {
            return _direction.ToString();
        }
        protected override string MsgType()
        {
            return "switchLane";
        }
    }
    #endregion
}
namespace Error
{
    #region Classes
    public class RootObject
    {
        public string msgType { get; set; }
        public string data { get; set; }
    }
    #endregion
}
namespace Crash
{
    #region Classes
    public class Data
    {
        public string name { get; set; }
        public string color { get; set; }
    }

    public class RootObject
    {
        public string msgType { get; set; }
        public Data data { get; set; }
        public string gameId { get; set; }
        public int gameTick { get; set; }
    }
    #endregion
}
namespace Spawn
{
    #region Classes
    public class Data
    {
        public string name { get; set; }
        public string color { get; set; }
    }

    public class RootObject
    {
        public string msgType { get; set; }
        public Data data { get; set; }
        public string gameId { get; set; }
        public int gameTick { get; set; }
    }
    #endregion
}
namespace LapFinished
{
    #region Classes
    public class Car
    {
        public string name { get; set; }
        public string color { get; set; }
    }

    public class LapTime
    {
        public int lap { get; set; }
        public int ticks { get; set; }
        public int millis { get; set; }
    }

    public class RaceTime
    {
        public int laps { get; set; }
        public int ticks { get; set; }
        public int millis { get; set; }
    }

    public class Ranking
    {
        public int overall { get; set; }
        public int fastestLap { get; set; }
    }

    public class Data
    {
        public Car car { get; set; }
        public LapTime lapTime { get; set; }
        public RaceTime raceTime { get; set; }
        public Ranking ranking { get; set; }
    }

    public class RootObject
    {
        public string msgType { get; set; }
        public Data data { get; set; }
        public string gameId { get; set; }
        public int gameTick { get; set; }
    }
    #endregion
}
namespace GameInit
{
    #region Classes
    public class Lane
    {
        public int distanceFromCenter { get; set; }
        public int index { get; set; }
        public double TrackLength { get; set; }
    }

    public class Position
    {
        public double x { get; set; }
        public double y { get; set; }
    }

    public class StartingPoint
    {
        public Position position { get; set; }
        public double angle { get; set; }
    }

    public class Track
    {
        public string id { get; set; }
        public string name { get; set; }
        public List<Piece> pieces { get; set; }
        public List<Lane> lanes { get; set; }
        public StartingPoint startingPoint { get; set; }
    }

    public class Id
    {
        public string name { get; set; }
        public string color { get; set; }
    }

    public class Dimensions
    {
        public double length { get; set; }
        public double width { get; set; }
        public double guideFlagPosition { get; set; }
    }

    public class Car
    {
        public Id id { get; set; }
        public Dimensions dimensions { get; set; }
    }

    public class RaceSession
    {
        public int laps { get; set; }
        public int maxLapTimeMs { get; set; }
        public bool quickRace { get; set; }
    }

    public class Race
    {
        public Track track { get; set; }
        public List<Car> cars { get; set; }
        public RaceSession raceSession { get; set; }
    }

    public class Data
    {
        public Race race { get; set; }
    }

    public class GameInit
    {
        public string msgType { get; set; }
        public Data data { get; set; }
    }
    #endregion
}
namespace CarPositions
{
    #region Classes
    public class Id
    {
        public string name { get; set; }
        public string color { get; set; }
    }

    public class Lane
    {
        public int startLaneIndex { get; set; }
        public int endLaneIndex { get; set; }
    }

    public class PiecePosition
    {
        public int pieceIndex { get; set; }
        public double inPieceDistance { get; set; }
        public Lane lane { get; set; }
        public int lap { get; set; }
    }

    public class Datum
    {
        public Id id { get; set; }
        public double angle { get; set; }
        public PiecePosition piecePosition { get; set; }
    }

    public class RootObject
    {
        public string msgType { get; set; }
        public List<Datum> data { get; set; }
        public string gameId { get; set; }
        public int gameTick { get; set; }
    }
    #endregion
}
namespace YourCar
{
    #region Classes
    public class Data
    {
        public string name { get; set; }
        public string color { get; set; }
    }

    public class RootObject
    {
        public string msgType { get; set; }
        public Data data { get; set; }
        public string gameId { get; set; }
    }
    public class YourCar
    {
        public string name { get; set; }
        public string color { get; set; }
    }
    #endregion
}
namespace TurboAvailable
{
    #region Classes
    public class Data
    {
        public double turboDurationMilliseconds { get; set; }
        public int turboDurationTicks { get; set; }
        public double turboFactor { get; set; }
    }

    public class RootObject
    {
        public string msgType { get; set; }
        public Data data { get; set; }
    }
    #endregion
}
#endregion